package ru.tinkoff.fintech.kanban;

import com.fasterxml.jackson.core.type.TypeReference;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.ResultActions;
import ru.tinkoff.fintech.ControllerTest;
import ru.tinkoff.fintech.kanban.card.Card;
import ru.tinkoff.fintech.kanban.status.Status;

import java.util.Collections;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class KanbanControllerTest extends ControllerTest<Kanban> {

    static {
        tableName = "kanbans";
        entityClass = Kanban.class;
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    void save() throws Exception {
        long boardId = 100;
        Kanban kanban = new Kanban(boardId,
                "kanban300",
                "d300",
                List.of(new Status("status100"), new Status("status101"))
        );

        adminRequest(performJson(post("/kanban"), kanban)).andExpect(status().isOk());

        long id = getIdByName(kanban.getName());

        expectKanban(adminRequest(get(String.format("/kanban/%d", id))), kanban);
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    void saveEmptyName() throws Exception {
        long boardId = 100;
        Kanban kanban = new Kanban(boardId,
                "",
                "d300",
                List.of(new Status("status100"), new Status("status101"))
        );

        adminRequest(performJson(post("/kanban"), kanban)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    void saveNullName() throws Exception {
        long boardId = 100;
        Kanban kanban = new Kanban(boardId,
                null,
                "d300",
                List.of(new Status("status100"), new Status("status101"))
        );

        adminRequest(performJson(post("/kanban"), kanban)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    void saveEmptyDescription() throws Exception {
        long boardId = 100;
        Kanban kanban = new Kanban(boardId,
                "kanban300",
                "",
                List.of(new Status("status100"), new Status("status101"))
        );

        adminRequest(performJson(post("/kanban"), kanban)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    void saveNullDescription() throws Exception {
        long boardId = 100;
        Kanban kanban = new Kanban(boardId,
                "kanban300",
                null,
                List.of(new Status("status100"), new Status("status101"))
        );

        adminRequest(performJson(post("/kanban"), kanban)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    void saveEmptyStatusList() throws Exception {
        long boardId = 100;
        Kanban kanban = new Kanban(boardId,
                "kanban300",
                "d300",
                Collections.emptyList()
        );

        adminRequest(performJson(post("/kanban"), kanban)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    void saveNullStatusList() throws Exception {
        long boardId = 100;
        Kanban kanban = new Kanban(boardId,
                "kanban300",
                "d300",
                null
        );

        adminRequest(performJson(post("/kanban"), kanban)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    void saveNonExistentBoard() throws Exception {
        long boardId = 105;
        Kanban kanban = new Kanban(boardId,
                "kanban300",
                "d300",
                null
        );

        adminRequest(performJson(post("/kanban"), kanban)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_statuses.sql")
    @Sql("/scripts/insert_kanbans.sql")
    void update() throws Exception {
        long boardId = 100;
        Kanban kanban = new Kanban(boardId,
                "newKanban",
                "newD",
                List.of(new Status("status104"), new Status("status105"))
        );
        long id = 300;
        kanban.setId(id);

        adminRequest(performJson(put("/kanban"), kanban)).andExpect(status().isOk());

        expectKanban(adminRequest(get(String.format("/kanban/%d", id))), kanban);
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_statuses.sql")
    @Sql("/scripts/insert_kanbans.sql")
    void updateNonExistentKanban() throws Exception {
        long boardId = 100;
        Kanban kanban = new Kanban(boardId,
                "newKanban",
                "newD",
                List.of(new Status("status104"), new Status("status105"))
        );
        long id = 303;
        kanban.setId(id);

        adminRequest(performJson(put("/kanban"), kanban)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_statuses.sql")
    @Sql("/scripts/insert_kanbans.sql")
    void updateEmptyName() throws Exception {
        long boardId = 100;
        Kanban kanban = new Kanban(boardId,
                "",
                "newD",
                List.of(new Status("status104"), new Status("status105"))
        );
        long id = 300;
        kanban.setId(id);

        adminRequest(performJson(put("/kanban"), kanban)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_statuses.sql")
    @Sql("/scripts/insert_kanbans.sql")
    void updateNullName() throws Exception {
        long boardId = 100;
        Kanban kanban = new Kanban(boardId,
                null,
                "newD",
                List.of(new Status("status104"), new Status("status105"))
        );
        long id = 300;
        kanban.setId(id);

        adminRequest(performJson(put("/kanban"), kanban)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_statuses.sql")
    @Sql("/scripts/insert_tags.sql")
    @Sql("/scripts/insert_kanbans.sql")
    void updateEmptyDescription() throws Exception {
        long boardId = 100;
        Kanban kanban = new Kanban(boardId,
                "newKanban",
                "",
                List.of(new Status("status104"), new Status("status105"))
        );
        long id = 300;
        kanban.setId(id);

        adminRequest(performJson(put("/kanban"), kanban)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_statuses.sql")
    @Sql("/scripts/insert_kanbans.sql")
    void updateNullDescription() throws Exception {
        long boardId = 100;
        Kanban kanban = new Kanban(boardId,
                "newKanban",
                null,
                List.of(new Status("status104"), new Status("status105"))
        );
        long id = 300;
        kanban.setId(id);

        adminRequest(performJson(put("/kanban"), kanban)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_statuses.sql")
    @Sql("/scripts/insert_kanbans.sql")
    void updateNullStatusList() throws Exception {
        long boardId = 100;
        Kanban kanban = new Kanban(boardId,
                "newKanban",
                "newD",
                null
        );
        long id = 300;
        kanban.setId(id);

        adminRequest(performJson(put("/kanban"), kanban)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_statuses.sql")
    @Sql("/scripts/insert_kanbans.sql")
    void updateEmptyStatusList() throws Exception {
        long boardId = 100;
        Kanban kanban = new Kanban(boardId,
                "newKanban",
                "newD",
                Collections.emptyList()
        );
        long id = 300;
        kanban.setId(id);

        adminRequest(performJson(put("/kanban"), kanban)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_statuses.sql")
    @Sql("/scripts/insert_tags.sql")
    @Sql("/scripts/insert_kanbans.sql")
    @Sql("/scripts/insert_cards.sql")
    void deleteById() throws Exception {
        long id = 300;
        adminRequest(delete(String.format("/kanban/%d", id))).andExpect(status().isOk());

        assertNotFound(id);
        assertEmpty("cards", "kanban_id", id);
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_statuses.sql")
    @Sql("/scripts/insert_tags.sql")
    @Sql("/scripts/insert_kanbans.sql")
    @Sql("/scripts/insert_cards.sql")
    void deleteByIdNotFound() throws Exception {
        adminRequest(delete("/kanban/305")).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_statuses.sql")
    @Sql("/scripts/insert_tags.sql")
    @Sql("/scripts/insert_kanbans.sql")
    @Sql("/scripts/insert_cards.sql")
    void deleteByIdTwice() throws Exception {
        long id = 300;
        adminRequest(delete(String.format("/kanban/%d", id))).andExpect(status().isOk());

        assertNotFound(id);
        assertEmpty("cards", "kanban_id", id);

        adminRequest(delete(String.format("/kanban/%d", id))).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_statuses.sql")
    @Sql("/scripts/insert_kanbans.sql")
    @Sql("/scripts/insert_tags.sql")
    @Sql("/scripts/insert_cards.sql")
    void getById() throws Exception {
        Card card400 = new Card();
        card400.setId(400L);
        Card card401 = new Card();
        card401.setId(401L);

        long boardId = 100;
        Kanban kanban = new Kanban(boardId,
                "kanban300",
                "d300",
                List.of(new Status("status100"), new Status("status101"))
        );
        kanban.setCards(List.of(card400, card401));

        long id = 300;
        kanban.setId(id);

        expectKanban(adminRequest(get(String.format("/kanban/%d", id))), kanban);
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_statuses.sql")
    @Sql("/scripts/insert_kanbans.sql")
    @Sql("/scripts/insert_tags.sql")
    @Sql("/scripts/insert_cards.sql")
    void getByIdNotFound() throws Exception {
        adminRequest(get("/kanban/305")).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_statuses.sql")
    @Sql("/scripts/insert_kanbans.sql")
    @Sql("/scripts/insert_tags.sql")
    @Sql("/scripts/insert_cards.sql")
    void getAll() throws Exception {
        Card card400 = new Card();
        card400.setId(400L);
        Card card401 = new Card();
        card401.setId(401L);

        long boardId = 100;
        Kanban kanban300 = new Kanban(boardId,
                "kanban300",
                "d300",
                List.of(new Status(100L, "status100"), new Status(101L, "status101"))
        );
        kanban300.setCards(List.of(card400, card401));
        kanban300.setId(300L);

        Kanban kanban301 = new Kanban(boardId,
                "kanban301",
                "d301",
                List.of(new Status(100L, "status100"), new Status(101L, "status101"))
        );
        kanban301.setId(301L);

        String content = adminRequest(get("/kanban")).andReturn().getResponse().getContentAsString();
        List<Kanban> actualKanbans = objectMapper.readValue(content, new TypeReference<>() {});
        actualKanbans.forEach(kanban -> {
            kanban.setCreatedBy(null);
            kanban.setLastModifiedBy(null);
            kanban.getCards().replaceAll(card -> {
                Card tmpCard = new Card();
                tmpCard.setId(card.getId());
                return tmpCard;
            });
        });

        Assertions.assertEquals(List.of(kanban300, kanban301), actualKanbans);
    }

    @Test
    void getAllEmpty() throws Exception {
        adminRequest(get("/kanban"))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(Collections.emptyList())));
    }

    protected void expectKanban(ResultActions resultActions, Kanban expectedKanban) throws Exception {
        resultActions.andExpect(status().isOk());

        String content = resultActions.andReturn().getResponse().getContentAsString();
        Kanban actualKanban = objectMapper.readValue(content, Kanban.class);

        assertEquals(expectedKanban, actualKanban);
    }

    private void assertEquals(Kanban expectedKanban, Kanban actualKanban) {
        Assertions.assertEquals(expectedKanban.getName(), actualKanban.getName());
        Assertions.assertEquals(expectedKanban.getDescription(), actualKanban.getDescription());
        Assertions.assertEquals(expectedKanban.getBoardId(), actualKanban.getBoardId());
        Assertions.assertEquals(getCardIds(expectedKanban), getCardIds(actualKanban));
        Assertions.assertEquals(getStatusNames(expectedKanban), getStatusNames(actualKanban));
    }

    private List<String> getStatusNames(Kanban kanban) {
        return kanban.getStatusList()
                .stream()
                .map(Status::getName)
                .toList();
    }

    private List<Long> getCardIds(Kanban kanban) {
        return kanban.getCards()
                .stream()
                .map(Card::getId)
                .toList();
    }
}
