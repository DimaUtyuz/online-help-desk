package ru.tinkoff.fintech.comment;

import com.fasterxml.jackson.core.type.TypeReference;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.ResultActions;
import ru.tinkoff.fintech.ControllerTest;
import ru.tinkoff.fintech.comment.message.Message;

import java.util.Collections;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class CommentControllerTest extends ControllerTest<Comment> {

    static {
        tableName = "comments";
        entityClass = Comment.class;
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    void save() throws Exception {
        long boardId = 100;
        Comment comment = new Comment(boardId,"comment500", "d500");

        adminRequest(performJson(post("/comment"), comment)).andExpect(status().isOk());

        long id = getIdByName(comment.getName());

        expectComment(adminRequest(get(String.format("/comment/%d", id))), comment);
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    void saveEmptyName() throws Exception {
        long boardId = 100;
        Comment comment = new Comment(boardId,"", "d500");

        adminRequest(performJson(post("/comment"), comment)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    void saveNullName() throws Exception {
        long boardId = 100;
        Comment comment = new Comment(boardId,null, "d500");

        adminRequest(performJson(post("/comment"), comment)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    void saveEmptyDescription() throws Exception {
        long boardId = 100;
        Comment comment = new Comment(boardId,"comment500", "");

        adminRequest(performJson(post("/comment"), comment)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    void saveNullDescription() throws Exception {
        long boardId = 100;
        Comment comment = new Comment(boardId, "comment500", null);

        adminRequest(performJson(post("/comment"), comment)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    void saveNonExistentBoard() throws Exception {
        long boardId = 110;
        Comment comment = new Comment(boardId, "comment500", "d500");

        adminRequest(performJson(post("/comment"), comment)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_comments.sql")
    void update() throws Exception {
        long boardId = 100;
        long id = 500;
        Comment comment = new Comment(boardId,"newComment", "newD");
        comment.setId(id);

        adminRequest(performJson(put("/comment"), comment)).andExpect(status().isOk());

        expectComment(adminRequest(get(String.format("/comment/%d", id))), comment);
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_comments.sql")
    void updateEmptyName() throws Exception {
        long boardId = 100;
        long id = 500;
        Comment comment = new Comment(boardId,"", "newD");
        comment.setId(id);

        adminRequest(performJson(put("/comment"), comment)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_comments.sql")
    void updateNullName() throws Exception {
        long boardId = 100;
        long id = 500;
        Comment comment = new Comment(boardId,null, "newD");
        comment.setId(id);

        adminRequest(performJson(put("/comment"), comment)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_comments.sql")
    void updateEmptyDescription() throws Exception {
        long boardId = 100;
        long id = 500;
        Comment comment = new Comment(boardId,"newComment", "");
        comment.setId(id);

        adminRequest(performJson(put("/comment"), comment)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_comments.sql")
    void updateNullDescription() throws Exception {
        long boardId = 100;
        long id = 500;
        Comment comment = new Comment(boardId,"newComment", null);
        comment.setId(id);

        adminRequest(performJson(put("/comment"), comment)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_comments.sql")
    void updateNotFound() throws Exception {
        long boardId = 100;
        long id = 510;
        Comment comment = new Comment(boardId,"newComment", "newD");
        comment.setId(id);

        adminRequest(performJson(put("/comment"), comment)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_comments.sql")
    void updateBoardId() throws Exception {
        long boardId = 100;
        long id = 500;
        Comment comment = new Comment(boardId + 1,"newComment", "newD");
        comment.setId(id);

        adminRequest(performJson(put("/comment"), comment)).andExpect(status().isOk());

        comment.setId(boardId);
        expectComment(adminRequest(get(String.format("/comment/%d", id))), comment);
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_comments.sql")
    @Sql("/scripts/insert_messages.sql")
    void deleteById() throws Exception {
        long id = 500;
        adminRequest(delete(String.format("/comment/%d", id))).andExpect(status().isOk());

        assertNotFound(id);
        assertEmpty("messages", "comment_id", id);
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_comments.sql")
    @Sql("/scripts/insert_messages.sql")
    void deleteByIdNotFound() throws Exception {
        long id = 510;
        adminRequest(delete(String.format("/comment/%d", id))).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_comments.sql")
    @Sql("/scripts/insert_messages.sql")
    void deleteByIdTwice() throws Exception {
        long id = 500;
        adminRequest(delete(String.format("/comment/%d", id))).andExpect(status().isOk());

        assertNotFound(id);
        assertEmpty("messages", "comment_id", id);

        adminRequest(delete(String.format("/comment/%d", id))).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_comments.sql")
    @Sql("/scripts/insert_messages.sql")
    void getById() throws Exception {
        Message message600 = new Message();
        message600.setId(600L);
        Message message602 = new Message();
        message602.setId(602L);

        long commentId = 500;
        long boardId = 100;
        Comment comment = new Comment(boardId, "comment500", "d500");
        comment.setResolved(false);
        comment.setId(commentId);
        comment.setMessageList(List.of(message600, message602));

        expectComment(adminRequest(get(String.format("/comment/%d", commentId))), comment);
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_comments.sql")
    @Sql("/scripts/insert_messages.sql")
    void getByIdNoyFound() throws Exception {
        long commentId = 510;
        adminRequest(get(String.format("/comment/%d", commentId))).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_comments.sql")
    @Sql("/scripts/insert_messages.sql")
    void getAll() throws Exception {
        Message message600 = new Message();
        message600.setId(600L);
        Message message601 = new Message();
        message601.setId(601L);
        Message message602 = new Message();
        message602.setId(602L);

        long boardId = 100;

        Comment comment500 = new Comment(boardId, "comment500", "d500");
        comment500.setResolved(false);
        comment500.setId(500L);
        comment500.setMessageList(List.of(message600, message602));

        Comment comment501 = new Comment(boardId, "comment501", "d501");
        comment501.setResolved(false);
        comment501.setId(501L);

        Comment comment502 = new Comment(boardId, "comment502", "d502");
        comment502.setResolved(true);
        comment502.setId(502L);
        comment502.setMessageList(List.of(message601));

        String content = adminRequest(get("/comment")).andReturn().getResponse().getContentAsString();
        List<Comment> actualComments = objectMapper.readValue(content, new TypeReference<>() {});
        actualComments.forEach(comment -> {
            comment.setCreatedBy(null);
            comment.setLastModifiedBy(null);
            comment.getMessageList().replaceAll(msg -> {
                Message tmpMsg = new Message();
                tmpMsg.setId(msg.getId());
                return tmpMsg;
            });
        });

        Assertions.assertEquals(List.of(comment500, comment501, comment502), actualComments);
    }

    @Test
    void getAllEmpty() throws Exception {
        adminRequest(get("/comment"))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(Collections.emptyList())));
    }

    protected void expectComment(ResultActions resultActions, Comment expectedComment) throws Exception {
        resultActions.andExpect(status().isOk());

        String content = resultActions.andReturn().getResponse().getContentAsString();
        Comment actualComment = objectMapper.readValue(content, Comment.class);

        assertEquals(expectedComment, actualComment);
    }

    private void assertEquals(Comment expectedComment, Comment actualComment) {
        Assertions.assertEquals(expectedComment.getName(), actualComment.getName());
        Assertions.assertEquals(expectedComment.getDescription(), actualComment.getDescription());
        Assertions.assertEquals(expectedComment.isResolved(), actualComment.isResolved());
        Assertions.assertEquals(getMessageIds(expectedComment), getMessageIds(actualComment));
    }

    private List<Long> getMessageIds(Comment comment) {
        return comment.getMessageList()
                .stream()
                .map(Message::getId)
                .toList();
    }

}
