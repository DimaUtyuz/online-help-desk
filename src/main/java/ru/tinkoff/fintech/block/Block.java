package ru.tinkoff.fintech.block;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.tinkoff.fintech.action.Action;
import ru.tinkoff.fintech.board.Board;

import javax.validation.constraints.NotBlank;

/**
 * Simple element that can be placed on the {@link Board}.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Block {

    protected Long id;

    @NotBlank(message = "Имя пусто.")
    protected String name;

    @NotBlank(message = "Описание пусто.")
    protected String description;

    protected Action createdBy;

    protected Action lastModifiedBy;

}
