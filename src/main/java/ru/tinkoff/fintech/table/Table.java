package ru.tinkoff.fintech.table;

import lombok.*;
import ru.tinkoff.fintech.block.Block;
import ru.tinkoff.fintech.board.Board;
import ru.tinkoff.fintech.form.Form;
import ru.tinkoff.fintech.form.validation.FormValid;
import ru.tinkoff.fintech.table.cell.Cell;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

/**
 * {@link Board} element that behaves as a simple table with {@link Cell}
 */
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@FormValid
public class Table extends Block implements Form {

    @NotNull(message = "Доска не задана")
    private Long boardId;

    private List<Cell> cells;

    @Min(value = 1, message = "Количество колонок должно быть больше 0")
    private int width;

    @Min(value = 1, message = "Количество строк должно быть больше 0")
    private int height;

    public Table(Long boardId, String name, String description, int width, int height) {
        this.name = name;
        this.description = description;
        this.boardId = boardId;
        this.width = width;
        this.height = height;
        this.cells = new ArrayList<>();
        addCells();
    }

    private void addCells() {
        for (int column = 0; column < width; column++) {
            for (int row = 0; row < height; row++) {
                this.cells.add(new Cell(column, row, ""));
            }
        }
    }

}
