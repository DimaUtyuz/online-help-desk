package ru.tinkoff.fintech.action;

import lombok.AllArgsConstructor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import ru.tinkoff.fintech.security.user.UserService;

import javax.persistence.EntityNotFoundException;

@Service
@AllArgsConstructor
public class ActionService {

    private final ActionRepository actionRepository;
    private final UserService userService;

    public Action createAction() {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        Action action = new Action(userService.getByLogin(username));
        add(action);
        return action;
    }

    public void add(Action action) {
        actionRepository.save(action);
    }

    public Action getById(Long id) {
        return actionRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Action with id: " + id + " not found"));
    }

}
