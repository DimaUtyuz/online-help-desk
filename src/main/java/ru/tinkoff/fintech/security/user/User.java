package ru.tinkoff.fintech.security.user;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.tinkoff.fintech.security.role.Role;

import javax.validation.constraints.NotBlank;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class User {

    private Long id;

    @NotBlank(message = "Имя пользователя не может быть пустым")
    private String login;

    private String password;

    private Role role;

    public User(String login, String password) {
        this.login = login;
        this.password = password;
    }
}
