package ru.tinkoff.fintech.kanban.card.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = CardValidator.class)
@Target({ElementType.TYPE, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
public @interface CardValid {

    String message() default "Kanban is non existent";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
