package ru.tinkoff.fintech.note.relation;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Relation {

    @NotNull(message = "Заметка не определена.")
    private Long fromNoteId;

    @NotNull(message = "Заметка не определена.")
    private Long toNoteId;

}
