package ru.tinkoff.fintech.kanban;

import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Optional;

@Mapper
public interface KanbanRepository {

    @Insert("""
            insert into kanbans (name, description, created_id, modified_id, board_id)
                values (#{name}, #{description}, #{createdBy.id}, #{lastModifiedBy.id}, #{boardId})
            """)
    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
    void save(Kanban kanban);

    @Select("""
            select k.id, k.name, k.description, k.created_id, k.modified_id, k.board_id
                from kanbans as k
            where k.id = #{id}
            """)
    @Results(id = "kanban", value = {
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "boardId", column = "board_id"),
            @Result(property = "createdBy", column = "created_id",
                    one = @One(select = "ru.tinkoff.fintech.action.ActionRepository.findById")),
            @Result(property = "lastModifiedBy", column = "modified_id",
                    one = @One(select = "ru.tinkoff.fintech.action.ActionRepository.findById")),
            @Result(property = "statusList", column = "id",
                    one = @One(select = "ru.tinkoff.fintech.kanban.status.StatusRepository.getByKanbanId")),
            @Result(property = "cards", column = "id",
                    one = @One(select = "ru.tinkoff.fintech.kanban.card.CardRepository.findByKanbanId"))
    })
    Optional<Kanban> findById(Long id);

    @Select("""
            select k.id, k.name, k.description, k.created_id, k.modified_id, k.board_id
                from kanbans as k
            """)
    @ResultMap("kanban")
    List<Kanban> findAll();

    @Select("""
            select k.id, k.name, k.description, k.created_id, k.modified_id, k.board_id
                from kanbans as k
            where k.board_id = #{boardId}
            """)
    @ResultMap("kanban")
    List<Kanban> findByBoardId(Long boardId);

    @Select("""
            select exists
                (select 1
                    from kanbans
                where id = #{id}
                )
            """)
    boolean isExistById(Long id);

    @Update("""
            update kanbans
                set name = #{name}, description = #{description}, modified_id = #{lastModifiedBy.id}
            where id = #{id}
            """)
    Long update(Kanban kanban);

    @Delete("""
            delete
                from kanbans
            where id = #{id}
            """)
    Long delete(Long id);

}
