package ru.tinkoff.fintech.comment;

import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Optional;

@Mapper
public interface CommentRepository {

    @Insert("""
            insert into comments (name, description, created_id, modified_id, board_id, resolved)
                values (#{name}, #{description}, #{createdBy.id},
                        #{lastModifiedBy.id}, #{boardId}, #{resolved})
            """)
    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
    void save(Comment comment);

    @Select("""
            select c.id, c.name, c.description, c.created_id, c.modified_id, c.board_id, c.resolved
                from comments as c
            where c.id = #{id}
            """)
    @Results(id = "comment", value = {
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "boardId", column = "board_id"),
            @Result(property = "resolved", column = "resolved"),
            @Result(property = "createdBy", column = "created_id",
                    one = @One(select = "ru.tinkoff.fintech.action.ActionRepository.findById")),
            @Result(property = "lastModifiedBy", column = "modified_id",
                    one = @One(select = "ru.tinkoff.fintech.action.ActionRepository.findById")),
            @Result(property = "messageList", column = "id",
                    many = @Many(select = "ru.tinkoff.fintech.comment.message.MessageRepository.findMessagesByCommentId"))
    })
    Optional<Comment> findById(Long id);

    @Select("""
            select c.id, c.name, c.description, c.created_id, c.modified_id, c.board_id, c.resolved
                from comments as c
            """)
    @ResultMap("comment")
    List<Comment> findAll();

    @Select("""
            select c.id, c.name, c.description, c.created_id, c.modified_id, c.board_id, c.resolved
                from comments as c
            where c.board_id = #{boardId}
            """)
    @ResultMap("comment")
    List<Comment> findByBoardId(Long boardId);

    @Update("""
            update comments
                set name = #{name}, description = #{description},
                    modified_id = #{lastModifiedBy.id}, resolved = #{resolved}
            where id = #{id}
            """)
    Long update(Comment comment);

    @Delete("""
            delete
                from comments
            where id = #{id}
            """)
    Long delete(Long id);

}
