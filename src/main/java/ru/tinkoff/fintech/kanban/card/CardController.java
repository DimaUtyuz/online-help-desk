package ru.tinkoff.fintech.kanban.card;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/card")
@AllArgsConstructor
public class CardController {

    private final CardService cardService;

    @PostMapping
    public void save(@RequestBody @Valid Card card) {
        cardService.add(card);
    }

    @GetMapping("/{id}")
    public Card getById(@PathVariable("id") Long id) {
        return cardService.getById(id);
    }

    @GetMapping("/tag/{tagId}")
    public List<Card> getByTagId(@PathVariable("tagId") Long tagId) {
        return cardService.getByTagId(tagId);
    }

    @PutMapping
    public void update(@RequestBody @Valid Card card) {
        cardService.update(card);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") Long id) {
        cardService.delete(id);
    }

}
