package ru.tinkoff.fintech.comment;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/comment")
@AllArgsConstructor
public class CommentController {

    private final CommentService commentService;

    @PostMapping
    public void save(@RequestBody @Valid Comment comment) {
        commentService.add(comment);
    }

    @GetMapping("/{id}")
    public Comment getById(@PathVariable("id") Long id) {
        return commentService.getById(id);
    }

    @GetMapping
    public List<Comment> getAll() {
        return commentService.getAll();
    }

    @PutMapping
    public void update(@RequestBody @Valid Comment comment) {
        commentService.update(comment);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") Long id) {
        commentService.delete(id);
    }

}
