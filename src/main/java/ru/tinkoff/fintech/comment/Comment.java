package ru.tinkoff.fintech.comment;

import lombok.*;
import ru.tinkoff.fintech.block.Block;
import ru.tinkoff.fintech.comment.message.Message;
import ru.tinkoff.fintech.form.Form;
import ru.tinkoff.fintech.form.validation.FormValid;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

/**
 * Discussion {@link Form} where you can send {@link Message}
 */
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@FormValid
public class Comment extends Block implements Form {

    @NotNull(message = "Доска не задана")
    private Long boardId;

    private boolean resolved;

    private List<Message> messageList;

    public Comment(Long boardId, String name, String description) {
        this.boardId = boardId;
        this.name = name;
        this.description = description;
        this.resolved = false;
        this.messageList = new ArrayList<>();
    }

}
