package ru.tinkoff.fintech.note;

import lombok.*;
import ru.tinkoff.fintech.block.Block;
import ru.tinkoff.fintech.board.Board;
import ru.tinkoff.fintech.form.Form;
import ru.tinkoff.fintech.form.validation.FormValid;
import ru.tinkoff.fintech.tag.Tag;
import ru.tinkoff.fintech.tag.Tagged;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

/**
 * {@link Board} element that has links to other notes
 */
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@FormValid
public class Note extends Block implements Form, Tagged {

    @NotNull(message = "Доска не задана")
    private Long boardId;

    @NotNull
    private List<Note> nextNotes;

    @NotNull(message = "Список тегов не установлен.")
    private List<Tag> tagList;

    public Note(Long boardId, String name, String description, List<Tag> tagList) {
        this.boardId = boardId;
        this.name = name;
        this.description = description;
        this.nextNotes = new ArrayList<>();
        this.tagList = tagList;
    }

}
