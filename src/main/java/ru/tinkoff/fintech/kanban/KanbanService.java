package ru.tinkoff.fintech.kanban;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tinkoff.fintech.action.aop.annotation.CreateBlockBy;
import ru.tinkoff.fintech.action.aop.annotation.ModifyBlockBy;
import ru.tinkoff.fintech.kanban.status.list.KanbanStatusesList;
import ru.tinkoff.fintech.kanban.status.list.KanbanStatusesService;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
@AllArgsConstructor
public class KanbanService {

    private final KanbanRepository kanbanRepository;
    private final KanbanStatusesService kanbanStatusesService;

    @Transactional
    @CreateBlockBy
    public void add(Kanban kanban) {
        kanbanRepository.save(kanban);

        addKanbanStatuses(kanban);
    }

    public Kanban getById(Long id) {
        return kanbanRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Kanban with id: " + id + " not found"));
    }

    public List<Kanban> getAll() {
        return kanbanRepository.findAll();
    }

    @Transactional
    public void delete(Long id) {
        if (kanbanRepository.delete(id) == 0) {
            throw new EntityNotFoundException("Kanban with id: " + id + " not found");
        }
    }

    @Transactional
    @ModifyBlockBy
    public void update(Kanban kanban) {
        if (kanbanRepository.update(kanban) == 0) {
            throw new EntityNotFoundException("Kanban with id: " + kanban.getId() + " not found");
        }

        addKanbanStatuses(kanban);
    }

    private void addKanbanStatuses(Kanban kanban) {
        KanbanStatusesList kanbanStatusesList = new KanbanStatusesList(kanban);
        kanbanStatusesService.add(kanbanStatusesList);
    }

}
