package ru.tinkoff.fintech.form.validation;

import org.springframework.beans.factory.annotation.Autowired;
import ru.tinkoff.fintech.board.BoardService;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class FormValidator implements ConstraintValidator<FormValid, ru.tinkoff.fintech.form.Form> {

    @Autowired
    BoardService boardService;

    @Override
    public boolean isValid(ru.tinkoff.fintech.form.Form form, ConstraintValidatorContext constraintValidatorContext) {
        return boardService.isExistById(form.getBoardId());
    }

}
