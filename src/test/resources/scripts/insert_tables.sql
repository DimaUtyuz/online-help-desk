insert into actions (id, user_id, creation_time)
    values (700, 100, now());

insert into tables (id, name, description, created_id, modified_id, board_id, width, height)
    values (700, 'table700', 'd700', 700, 700, 100, 2, 2);

insert into actions (id, user_id, creation_time)
    values (701, 100, now());

insert into tables (id, name, description, created_id, modified_id, board_id, width, height)
    values (701, 'table701', 'd701', 701, 701, 100, 1, 1);
