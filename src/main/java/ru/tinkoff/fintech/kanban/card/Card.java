package ru.tinkoff.fintech.kanban.card;

import lombok.*;
import ru.tinkoff.fintech.block.Block;
import ru.tinkoff.fintech.kanban.Kanban;
import ru.tinkoff.fintech.kanban.card.validation.CardValid;
import ru.tinkoff.fintech.kanban.status.Status;
import ru.tinkoff.fintech.security.user.User;
import ru.tinkoff.fintech.tag.Tag;
import ru.tinkoff.fintech.tag.Tagged;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * {@link Kanban} card.
 */
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@CardValid
public class Card extends Block implements Tagged {

    @NotNull(message = "Канбан-доска не задана")
    private Long kanbanId;

    @NotNull(message = "Статус не задан.")
    private Status status;

    @NotNull(message = "Никто не назначен на задачу.")
    private User assignee;

    @NotNull(message = "Список тегов не установлен.")
    private List<Tag> tagList;

    public Card(Long kanbanId, String name, String description, Status status, User assignee, List<Tag> tagList) {
        this.kanbanId = kanbanId;
        this.name = name;
        this.description = description;
        this.status = status;
        this.assignee = assignee;
        this.tagList = tagList;
    }
}
