insert into users (id, login, password, role_id)
    values (1, 's1', '$2a$10$/ZF9sufywhiIWRTc3AL6S.yXBovk0l4J42V9i0mXVbvJCRtqBEGRi', 2);
insert into users (id, login, password, role_id)
    values (2, 's2', '$2a$10$YOV.mq4AyJo0/yLvKgMcfelKF8lYgVfMlOc9w.sulxDJKkjnkYZli', 2);
insert into users (id, login, password, role_id)
    values (3, 's3', '$2a$10$l.7OfYXs6DZ4pOtfD6P2FOw4v/enQRjvMID8NaIdVHz.yr59dRCTq', 2);
