package ru.tinkoff.fintech.security.role;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import java.util.Optional;

@Mapper
public interface RoleRepository {

    @Select("""
            select roles.id, roles.name
                from roles
            where roles.id = #{id}
            """)
    @Results(id = "role", value = {
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name")
    })
    Optional<Role> findById(Long id);
}
