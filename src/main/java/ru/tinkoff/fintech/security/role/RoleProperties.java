package ru.tinkoff.fintech.security.role;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties(prefix = "security.role")
public class RoleProperties {
    private Role admin;
    private Role user;
}
