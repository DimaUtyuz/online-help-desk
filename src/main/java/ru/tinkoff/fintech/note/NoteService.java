package ru.tinkoff.fintech.note;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tinkoff.fintech.action.aop.annotation.CreateBlockBy;
import ru.tinkoff.fintech.action.aop.annotation.ModifyBlockBy;
import ru.tinkoff.fintech.note.relation.Relation;
import ru.tinkoff.fintech.note.relation.RelationService;
import ru.tinkoff.fintech.tag.Tag;
import ru.tinkoff.fintech.tag.list.BlockTagsList;
import ru.tinkoff.fintech.tag.list.BlockTagsService;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
@AllArgsConstructor
public class NoteService {

    private final NoteRepository noteRepository;
    private final RelationService relationService;
    private final BlockTagsService blockTagsService;

    @Transactional
    @CreateBlockBy
    public void add(Note note) {
        noteRepository.save(note);
        addBlockTags(note);
    }

    public Note getById(Long id) {
        return noteRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Note with id: " + id + " not found"));
    }

    @Transactional
    public List<Note> getByTagId(Long tagId) {
        List<Long> noteIdList = blockTagsService.getBlockIdsByTagId(tagId);
        return noteIdList.stream().map(this::getById).toList();
    }

    public List<Note> getAll() {
        return noteRepository.findAll();
    }

    @Transactional
    public void delete(Long id) {
        blockTagsService.deleteBlock(id);
        if (noteRepository.delete(id) == 0) {
            throw new EntityNotFoundException("Note with id: " + id + " not found");
        }
    }

    @Transactional
    @ModifyBlockBy
    public void update(Note note) {
        if (noteRepository.update(note) == 0) {
            throw new EntityNotFoundException("Note with id: " + note.getId() + " not found");
        }

        addBlockTags(note);
    }

    public void addRelation(Relation relation) {
        relationService.add(relation);
    }

    public void deleteRelation(Relation relation) {
        relationService.delete(relation);
    }

    private void addBlockTags(Note note) {
        List<Tag> tagList = note.getTagList();
        BlockTagsList blockTagsList = new BlockTagsList(tagList, note.getId());
        blockTagsService.addBlockTags(blockTagsList);
    }

}
