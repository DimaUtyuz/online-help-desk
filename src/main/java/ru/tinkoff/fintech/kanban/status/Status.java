package ru.tinkoff.fintech.kanban.status;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Status {

    private Long id;

    @NotBlank(message = "Статус не задан.")
    private String name;

    public Status(String name) {
        this.name = name;
    }
}
