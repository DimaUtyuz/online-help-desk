package ru.tinkoff.fintech.table.cell;

import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Optional;

@Mapper
public interface CellRepository {

    @Insert("""
            insert into cells (table_id, "column", "row", "text")
                values (#{tableId}, #{cell.column}, #{cell.row}, #{cell.text})
            """)
    @Options(useGeneratedKeys = true, keyProperty = "cell.id", keyColumn = "id")
    void save(Cell cell, Long tableId);

    @Select("""
            select c.id, c.column, c.row, c.text
                from cells as c
            where c.id = #{id}
            """)
    @Results(id = "cell", value = {
            @Result(property = "id", column = "id"),
            @Result(property = "column", column = "column"),
            @Result(property = "row", column = "row"),
            @Result(property = "text", column = "text")
    })
    Optional<Cell> findById(Long id);

    @Select("""
            select c.id, c.column, c.row, c.text
                from cells as c
            where c.table_id = #{id}
            """)
    @ResultMap("cell")
    List<Cell> findByTableId(Long tableId);

    @Update("""
            update cells
                set text = #{text}
            where id = #{id}
            """)
    Long update(Cell cell);

    @Delete("""
            delete
                from cells as c
            where c.table_id = #{tableId} AND c.column = #{column}
            """)
    Long deleteColumn(Long tableId, int column);

    @Delete("""
            delete
                from cells as c
            where c.table_id = #{tableId} AND c.row = #{row}
            """)
    Long deleteRow(Long tableId, int row);

}
