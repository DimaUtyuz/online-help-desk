package ru.tinkoff.fintech.table.cell;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ru.tinkoff.fintech.table.Table;

import javax.persistence.EntityNotFoundException;

@Service
@AllArgsConstructor
public class CellService {

    private final CellRepository cellRepository;

    public void add(Cell cell, Long tableId) {
        cellRepository.save(cell, tableId);
    }

    public Cell getById(Long id) {
        return cellRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Cell with id: " + id + " not found"));
    }

    public void update(Cell cell) {
        if (cellRepository.update(cell) == 0) {
            throw new EntityNotFoundException("Cell with id: " + cell.getId() + " not found");
        }
    }

    public void deleteColumn(Long tableId, int column) {
        if (cellRepository.deleteColumn(tableId, column) == 0) {
            throw new EntityNotFoundException("Cells in column: " + column + " from table with id: " + tableId + " not found");
        }
    }

    public void deleteRow(Long tableId, int row) {
        if (cellRepository.deleteRow(tableId, row) == 0) {
            throw new EntityNotFoundException("Cells in row: " + row + " from table with id: " + tableId + " not found");
        }
    }

    public void addCells(Table table) {
        for (int row = 0; row < table.getHeight(); row++) {
            for (int column = 0; column < table.getWidth(); column++) {
                Cell cell = new Cell(column, row, "");
                add(cell, table.getId());
            }
        }
    }

}
