package ru.tinkoff.fintech.table.cell;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/cell")
@AllArgsConstructor
public class CellController {

    private final CellService cellService;

    @PutMapping
    public void update(@RequestBody @Valid Cell cell) {
        cellService.update(cell);
    }

    @GetMapping("/{id}")
    public Cell get(@PathVariable("id") Long id) {
        return cellService.getById(id);
    }

}
