package ru.tinkoff.fintech.security.role;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Role {

    private Long id;

    @NotBlank(message = "Имя не может быть пустым")
    private String name;

}
