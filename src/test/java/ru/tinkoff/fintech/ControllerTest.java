package ru.tinkoff.fintech;

import org.junit.jupiter.api.Assertions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import java.util.List;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;

@AutoConfigureMockMvc
public class ControllerTest<T> extends AbstractIntegrationTest {

    @Autowired
    protected MockMvc mockMvc;

    @Autowired
    protected JdbcTemplate jdbcTemplate;

    protected static String tableName;

    protected static Class<?> entityClass;

    protected ResultActions adminRequest(MockHttpServletRequestBuilder builder) throws Exception {
        return mockMvc.perform(builder.with(httpBasic("admin", "admin")));
    }

    protected ResultActions userRequest(MockHttpServletRequestBuilder builder) throws Exception {
        return mockMvc.perform(builder.with(httpBasic("user", "user")));
    }

    protected ResultActions nonExistentUserRequest(MockHttpServletRequestBuilder builder) throws Exception {
        return mockMvc.perform(builder.with(httpBasic("user1", "user1")));
    }

    protected MockHttpServletRequestBuilder performJson(MockHttpServletRequestBuilder builder, Object o) throws Exception {
        String json = objectMapper.writeValueAsString(o);

        return builder
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(json);
    }

    protected void assertEmpty(String columnName, long value) {
        assertEmpty(tableName, columnName, value);
    }

    protected void assertEmpty(String tableName, String columnName, long value) {
        Integer actualSize = jdbcTemplate.queryForObject(String.format("select count(*) from %s where %s = %d",
                        tableName, columnName, value),
                Integer.class);
        Assertions.assertEquals(0, actualSize);
    }

    protected void assertNotFound(long id) {
        List<?> actualList = jdbcTemplate.queryForList(String.format("select * from %s where id=%d", tableName, id), entityClass);
        Assertions.assertTrue(actualList.isEmpty());
    }

    protected long getIdByName(String name) {
        Integer id = jdbcTemplate.queryForObject(String.format("select id from %s where name = '%s'", tableName, name), Integer.class);
        if (id == null) {
            return -1;
        }
        return id;
    }

    protected long getIdByColumnAndValue(String columnName, String value) {
        Integer id = jdbcTemplate.queryForObject(String.format("select id from %s where %s = '%s'", tableName, columnName, value), Integer.class);
        if (id == null) {
            return -1;
        }
        return id;
    }

}
