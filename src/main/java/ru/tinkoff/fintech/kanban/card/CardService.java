package ru.tinkoff.fintech.kanban.card;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tinkoff.fintech.action.aop.annotation.CreateBlockBy;
import ru.tinkoff.fintech.action.aop.annotation.ModifyBlockBy;
import ru.tinkoff.fintech.tag.Tag;
import ru.tinkoff.fintech.tag.list.BlockTagsList;
import ru.tinkoff.fintech.tag.list.BlockTagsService;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
@AllArgsConstructor
public class CardService {

    private final CardRepository cardRepository;
    private final BlockTagsService blockTagsService;

    @Transactional
    @CreateBlockBy
    public void add(Card card) {
        cardRepository.save(card);
        addBlockTags(card);
    }

    public Card getById(Long id) {
        return cardRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Card with id: " + id + " not found"));
    }

    @Transactional
    public List<Card> getByTagId(Long tagId) {
        List<Long> cardIdList = blockTagsService.getBlockIdsByTagId(tagId);
        return cardIdList.stream().map(this::getById).toList();
    }

    public List<Card> getAll() {
        return cardRepository.findAll();
    }

    @Transactional
    public void delete(Long id) {
        blockTagsService.deleteBlock(id);
        if (cardRepository.delete(id) == 0) {
            throw new EntityNotFoundException("Card with id: " + id + " not found");
        }
    }

    @Transactional
    @ModifyBlockBy
    public void update(Card card) {
        if (cardRepository.update(card) == 0) {
            throw new EntityNotFoundException("Card with id: " + card.getId() + " not found");
        }

        addBlockTags(card);
    }

    private void addBlockTags(Card card) {
        List<Tag> tagList = card.getTagList();
        BlockTagsList blockTagsList = new BlockTagsList(tagList, card.getId());
        blockTagsService.addBlockTags(blockTagsList);
    }

}
