package ru.tinkoff.fintech.kanban;

import lombok.*;
import ru.tinkoff.fintech.block.Block;
import ru.tinkoff.fintech.form.Form;
import ru.tinkoff.fintech.form.validation.FormValid;
import ru.tinkoff.fintech.kanban.card.Card;
import ru.tinkoff.fintech.kanban.status.Status;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

/**
 * Special board for storing {@link Card}s and tracking their solution.
 * <a href="https://habr.com/ru/post/230725/">What is kanban?</a>
 */
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@FormValid
public class Kanban extends Block implements Form {

    @NotNull(message = "Доска не задана")
    private Long boardId;

    @NotEmpty(message = "Список статусов пуст")
    private List<Status> statusList;

    private List<Card> cards;

    public Kanban(Long boardId, String name, String description, List<Status> statusList) {
        this.boardId = boardId;
        this.name = name;
        this.description = description;
        this.statusList = statusList;
        this.cards = new ArrayList<>();
    }

}
