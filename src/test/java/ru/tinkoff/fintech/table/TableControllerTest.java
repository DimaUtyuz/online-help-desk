package ru.tinkoff.fintech.table;

import com.fasterxml.jackson.core.type.TypeReference;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.ResultActions;
import ru.tinkoff.fintech.ControllerTest;
import ru.tinkoff.fintech.table.cell.Cell;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class TableControllerTest extends ControllerTest<Table> {

    static {
        tableName = "tables";
        entityClass = Table.class;
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    void save() throws Exception {
        long boardId = 100;
        Table table = new Table(boardId, "table700", "d700", 2, 2);

        adminRequest(performJson(post("/table"), table)).andExpect(status().isOk());

        long id = getIdByName(table.getName());

        expectTable(adminRequest(get(String.format("/table/%d", id))), table);
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    void saveNonExistentBoard() throws Exception {
        long boardId = 110;
        Table table = new Table(boardId, "table700", "d700", 2, 2);

        adminRequest(performJson(post("/table"), table)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    void saveEmptyName() throws Exception {
        long boardId = 100;
        Table table = new Table(boardId, "", "d700", 2, 2);

        adminRequest(performJson(post("/table"), table)).andExpect(status().is4xxClientError());
    }


    @Test
    @Sql("/scripts/insert_boards.sql")
    void saveNullName() throws Exception {
        long boardId = 100;
        Table table = new Table(boardId, null, "d700", 2, 2);

        adminRequest(performJson(post("/table"), table)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    void saveEmptyDescription() throws Exception {
        long boardId = 100;
        Table table = new Table(boardId, "table700", "", 2, 2);

        adminRequest(performJson(post("/table"), table)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    void saveNullDescription() throws Exception {
        long boardId = 100;
        Table table = new Table(boardId, "table700", null, 2, 2);

        adminRequest(performJson(post("/table"), table)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    void saveZeroWidth() throws Exception {
        long boardId = 100;
        Table table = new Table(boardId, "table700", "d700", 0, 2);

        adminRequest(performJson(post("/table"), table)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    void saveZeroHeight() throws Exception {
        long boardId = 100;
        Table table = new Table(boardId, "table700", "d700", 2, 0);

        adminRequest(performJson(post("/table"), table)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    void saveNegativeWidth() throws Exception {
        long boardId = 100;
        Table table = new Table(boardId, "table700", "d700", -1, 2);

        adminRequest(performJson(post("/table"), table)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    void saveNegativeHeight() throws Exception {
        long boardId = 100;
        Table table = new Table(boardId, "table700", "d700", 2, -1);

        adminRequest(performJson(post("/table"), table)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_tables.sql")
    @Sql("/scripts/insert_cells.sql")
    void update() throws Exception {
        long boardId = 100;
        long id = 700;
        Table table = new Table(boardId, "newTable", "newD", 2, 2);
        table.setId(id);
        table.setCells(List.of(new Cell(1, 1, "text11"), new Cell(1, 2, "text12"),
                new Cell(2, 1, "text21"), new Cell(2, 2, "text22"))
        );

        adminRequest(performJson(put("/table"), table)).andExpect(status().isOk());

        expectTable(adminRequest(get(String.format("/table/%d", id))), table);
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_tables.sql")
    @Sql("/scripts/insert_cells.sql")
    void updateNonExistentTable() throws Exception {
        long boardId = 100;
        long id = 710;
        Table table = new Table(boardId, "newTable", "newD", 2, 2);
        table.setId(id);
        table.setCells(List.of(new Cell(1, 1, "text11"), new Cell(1, 2, "text12"),
                new Cell(2, 1, "text21"), new Cell(2, 2, "text22"))
        );

        adminRequest(performJson(put("/table"), table)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_tables.sql")
    @Sql("/scripts/insert_cells.sql")
    void updateEmptyName() throws Exception {
        long boardId = 100;
        long id = 700;
        Table table = new Table(boardId, "", "newD", 2, 2);
        table.setId(id);
        table.setCells(List.of(new Cell(1, 1, "text11"), new Cell(1, 2, "text12"),
                new Cell(2, 1, "text21"), new Cell(2, 2, "text22"))
        );

        adminRequest(performJson(put("/table"), table)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_tables.sql")
    @Sql("/scripts/insert_cells.sql")
    void updateNullName() throws Exception {
        long boardId = 100;
        long id = 700;
        Table table = new Table(boardId, null, "newD", 2, 2);
        table.setId(id);
        table.setCells(List.of(new Cell(1, 1, "text11"), new Cell(1, 2, "text12"),
                new Cell(2, 1, "text21"), new Cell(2, 2, "text22"))
        );

        adminRequest(performJson(put("/table"), table)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_tables.sql")
    @Sql("/scripts/insert_cells.sql")
    void updateEmptyDescription() throws Exception {
        long boardId = 100;
        long id = 700;
        Table table = new Table(boardId, "newTable", "", 2, 2);
        table.setId(id);
        table.setCells(List.of(new Cell(1, 1, "text11"), new Cell(1, 2, "text12"),
                new Cell(2, 1, "text21"), new Cell(2, 2, "text22"))
        );

        adminRequest(performJson(put("/table"), table)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_tables.sql")
    @Sql("/scripts/insert_cells.sql")
    void updateNullDescription() throws Exception {
        long boardId = 100;
        long id = 700;
        Table table = new Table(boardId, "newTable", null, 2, 2);
        table.setId(id);
        table.setCells(List.of(new Cell(1, 1, "text11"), new Cell(1, 2, "text12"),
                new Cell(2, 1, "text21"), new Cell(2, 2, "text22"))
        );

        adminRequest(performJson(put("/table"), table)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_tables.sql")
    @Sql("/scripts/insert_cells.sql")
    void updateIgnoreChanges() throws Exception {
        long boardId = 100;
        long id = 700;
        Table table = new Table(boardId, "table700", "d700", 2, 2);
        table.setId(id);
        table.setCells(List.of(new Cell(1, 1, "text11"), new Cell(1, 2, "text12"),
                new Cell(2, 1, "text21"), new Cell(2, 2, "text22"))
        );

        Table changedTable = new Table(boardId + 1, "table700", "d700", 1, 1);
        changedTable.setId(id);
        changedTable.setCells(List.of(new Cell(1, 1, "newText")));

        adminRequest(performJson(put("/table"), changedTable)).andExpect(status().isOk());

        expectTable(adminRequest(get(String.format("/table/%d", id))), table);
    }


    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_tables.sql")
    @Sql("/scripts/insert_cells.sql")
    void deleteById() throws Exception {
        long id = 700;
        adminRequest(delete(String.format("/table/%d", id))).andExpect(status().isOk());

        assertNotFound(id);
        assertEmpty("cells", "table_id", id);
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_tables.sql")
    @Sql("/scripts/insert_cells.sql")
    void deleteByIdNotFound() throws Exception {
        long id = 710;
        adminRequest(delete(String.format("/table/%d", id))).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_tables.sql")
    @Sql("/scripts/insert_cells.sql")
    void deleteByIdTwice() throws Exception {
        long id = 700;
        adminRequest(delete(String.format("/table/%d", id))).andExpect(status().isOk());

        assertNotFound(id);
        assertEmpty("cells", "table_id", id);

        adminRequest(delete(String.format("/table/%d", id))).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_tables.sql")
    @Sql("/scripts/insert_cells.sql")
    void getById() throws Exception {
        long boardId = 100;
        long id = 700;
        Table table = new Table(boardId, "table700", "d700", 2, 2);
        table.setId(id);
        table.setCells(List.of(new Cell(1, 1, "text11"), new Cell(1, 2, "text12"),
                new Cell(2, 1, "text21"), new Cell(2, 2, "text22"))
        );

        expectTable(adminRequest(get(String.format("/table/%d", id))), table);
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_tables.sql")
    @Sql("/scripts/insert_cells.sql")
    void getByIdNotFound() throws Exception {
        long id = 710;
        adminRequest(get(String.format("/table/%d", id))).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_tables.sql")
    @Sql("/scripts/insert_cells.sql")
    void getAll() throws Exception {
        long boardId = 100;

        Table table700 = new Table(boardId, "table700", "d700", 2, 2);
        table700.setId(700L);
        table700.setCells(List.of(new Cell(800L, 1, 1, "text11"), new Cell(801L, 1, 2, "text12"),
                new Cell(802L, 2, 1, "text21"), new Cell(803L, 2, 2, "text22"))
        );

        Table table701 = new Table(boardId, "table701", "d701", 1, 1);
        table701.setId(701L);
        table701.setCells(List.of(new Cell(804L, 1, 1, "text11")));

        String content = adminRequest(get("/table")).andReturn().getResponse().getContentAsString();
        List<Table> actualTables = objectMapper.readValue(content, new TypeReference<>() {
        });
        actualTables.forEach(table -> {
            table.setCreatedBy(null);
            table.setLastModifiedBy(null);
        });

        Assertions.assertEquals(List.of(table700, table701), actualTables);
    }

    @Test
    void getAllEmpty() throws Exception {
        adminRequest(get("/table"))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(Collections.emptyList())));
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_tables.sql")
    @Sql("/scripts/insert_cells.sql")
    void addColumn() throws Exception {
        long boardId = 100;
        long id = 701;
        adminRequest(put(String.format("/table/%d/column/add", id))).andExpect(status().isOk());

        Table table = new Table(boardId, "table701", "d701", 2, 1);
        table.setId(701L);
        table.setCells(List.of(new Cell(1, 1, "text11"), new Cell(2, 1, "")));

        expectTable(adminRequest(get(String.format("/table/%d", id))), table);
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_tables.sql")
    @Sql("/scripts/insert_cells.sql")
    void addRow() throws Exception {
        long boardId = 100;
        long id = 701;
        adminRequest(put(String.format("/table/%d/row/add", id))).andExpect(status().isOk());

        Table table = new Table(boardId, "table701", "d701", 1, 2);
        table.setId(701L);
        table.setCells(List.of(new Cell(1, 1, "text11"), new Cell(1, 2, "")));

        expectTable(adminRequest(get(String.format("/table/%d", id))), table);
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_tables.sql")
    @Sql("/scripts/insert_cells.sql")
    void addColumnNotFound() throws Exception {
        long id = 710;
        adminRequest(put(String.format("/table/%d/column/add", id))).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_tables.sql")
    @Sql("/scripts/insert_cells.sql")
    void addRowNotFound() throws Exception {
        long id = 710;
        adminRequest(put(String.format("/table/%d/row/add", id))).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_tables.sql")
    @Sql("/scripts/insert_cells.sql")
    void deleteColumn() throws Exception {
        long boardId = 100;
        long id = 700;
        adminRequest(put(String.format("/table/%d/column/delete", id))).andExpect(status().isOk());

        Table table = new Table(boardId, "table700", "d700", 1, 2);
        table.setId(id);
        table.setCells(List.of(new Cell(1, 1, "text11"), new Cell(1, 2, "text12")));

        expectTable(adminRequest(get(String.format("/table/%d", id))), table);
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_tables.sql")
    @Sql("/scripts/insert_cells.sql")
    void deleteRow() throws Exception {
        long boardId = 100;
        long id = 700;
        adminRequest(put(String.format("/table/%d/row/delete", id))).andExpect(status().isOk());

        Table table = new Table(boardId, "table700", "d700", 2, 1);
        table.setId(id);
        table.setCells(List.of(new Cell(1, 1, "text11"), new Cell(2, 1, "text21")));

        expectTable(adminRequest(get(String.format("/table/%d", id))), table);
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_tables.sql")
    @Sql("/scripts/insert_cells.sql")
    void deleteSingleColumn() throws Exception {
        long id = 701;
        adminRequest(put(String.format("/table/%d/column/delete", id))).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_tables.sql")
    @Sql("/scripts/insert_cells.sql")
    void deleteSingleRow() throws Exception {
        long id = 701;
        adminRequest(put(String.format("/table/%d/row/delete", id))).andExpect(status().is4xxClientError());
    }

    protected void expectTable(ResultActions resultActions, Table expectedTable) throws Exception {
        resultActions.andExpect(status().isOk());

        String content = resultActions.andReturn().getResponse().getContentAsString();
        Table actualTable = objectMapper.readValue(content, Table.class);

        assertEquals(expectedTable, actualTable);
    }

    private void assertEquals(Table expectedTable, Table actualTable) {
        Assertions.assertEquals(expectedTable.getName(), actualTable.getName());
        Assertions.assertEquals(expectedTable.getDescription(), actualTable.getDescription());
        Assertions.assertEquals(expectedTable.getHeight(), actualTable.getHeight());
        Assertions.assertEquals(expectedTable.getWidth(), actualTable.getWidth());
        Assertions.assertEquals(expectedTable.getBoardId(), actualTable.getBoardId());
        Assertions.assertEquals(getCellTexts(expectedTable), getCellTexts(actualTable));
    }

    private Set<String> getCellTexts(Table table) {
        return table.getCells()
                .stream()
                .map(Cell::getText)
                .collect(Collectors.toSet());
    }

}
