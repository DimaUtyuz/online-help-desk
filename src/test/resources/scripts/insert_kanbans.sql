insert into actions (id, user_id, creation_time)
    values (300, 100, now());

insert into kanbans (id, name, description, created_id, modified_id, board_id)
    values (300, 'kanban300', 'd300', 300, 300, 100);

insert into actions (id, user_id, creation_time)
    values (301, 100, now());

insert into kanbans (id, name, description, created_id, modified_id, board_id)
    values (301, 'kanban301', 'd301', 301, 301, 100);

insert into status_kanban (status_id, kanban_id)
    values (100, 300);

insert into status_kanban (status_id, kanban_id)
    values (101, 300);

insert into status_kanban (status_id, kanban_id)
    values (100, 301);

insert into status_kanban (status_id, kanban_id)
    values (101, 301);
