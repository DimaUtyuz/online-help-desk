package ru.tinkoff.fintech.board;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tinkoff.fintech.action.aop.annotation.CreateBlockBy;
import ru.tinkoff.fintech.action.aop.annotation.ModifyBlockBy;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
@AllArgsConstructor
public class BoardService {

    private final BoardRepository boardRepository;

    @Transactional
    @CreateBlockBy
    public void add(Board board) {
        boardRepository.save(board);
    }

    public Board getById(Long id) {
        return boardRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Board with id: " + id + " not found"));
    }

    public boolean isExistById(Long id) {
        return boardRepository.isExistById(id);
    }

    public List<Board> getAll() {
        return boardRepository.findAll();
    }

    public void delete(Long id) {
        if (boardRepository.delete(id) == 0) {
            throw new EntityNotFoundException("Board with id: " + id + " not found");
        }
    }

    @Transactional
    @ModifyBlockBy
    public void update(Board board) {
        if (boardRepository.update(board) == 0) {
            throw new EntityNotFoundException("Board with id: " + board.getId() + " not found");
        }
    }

}
