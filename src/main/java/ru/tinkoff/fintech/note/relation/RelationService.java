package ru.tinkoff.fintech.note.relation;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;

@Service
@AllArgsConstructor
public class RelationService {

    private final RelationRepository relationRepository;

    public void add(Relation relation) {
        try {
            relationRepository.save(relation);
        } catch (RuntimeException e) {
            throw new IllegalArgumentException(
                    String.format("Relation between notes : %d, %d can not be created",
                            relation.getFromNoteId(), relation.getToNoteId())
            );
        }
    }

    public void delete(Relation relation) {
        if (relationRepository.delete(relation) == 0) {
            throw new EntityNotFoundException(
                    String.format("Relation between notes: %d to %d not found",
                            relation.getFromNoteId(), relation.getToNoteId())
            );
        }
    }

}
