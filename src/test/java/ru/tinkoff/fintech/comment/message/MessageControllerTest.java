package ru.tinkoff.fintech.comment.message;

import com.fasterxml.jackson.core.type.TypeReference;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.ResultActions;
import ru.tinkoff.fintech.ControllerTest;
import ru.tinkoff.fintech.action.Action;
import ru.tinkoff.fintech.security.user.User;

import java.util.Collections;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class MessageControllerTest extends ControllerTest<Message> {

    static {
        tableName = "messages";
        entityClass = Message.class;
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_comments.sql")
    void saveSuccess() throws Exception {
        long commentId = 500;
        Message message = new Message(commentId, "message100");

        adminRequest(performJson(post("/message"), message)).andExpect(status().isOk());

        message.setCreatedBy(new Action(new User(100L, null, null, null)));
        long id = getIdByColumnAndValue("text", message.getText());

        expectMessage(adminRequest(get(String.format("/message/%d", id))), message);
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_comments.sql")
    void saveNonExistentComment() throws Exception {
        long commentId = 505;
        Message message = new Message(commentId,"message100");

        adminRequest(performJson(post("/message"), message)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_comments.sql")
    void saveNullComment() throws Exception {
        Message message = new Message(null,"message100");

        adminRequest(performJson(post("/message"), message)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_comments.sql")
    void saveEmptyText() throws Exception {
        long commentId = 500;
        Message message = new Message(commentId,"");

        adminRequest(performJson(post("/message"), message)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_comments.sql")
    void saveNullText() throws Exception {
        long commentId = 500;
        Message message = new Message(commentId,null);

        adminRequest(performJson(post("/message"), message)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_comments.sql")
    void saveResolvedComment() throws Exception {
        long commentId = 502;
        Message message = new Message(commentId,"message100");

        adminRequest(performJson(post("/message"), message)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_comments.sql")
    @Sql("/scripts/insert_messages.sql")
    void updateSuccess() throws Exception {
        long commentId = 500;
        Message message = new Message(600L, commentId, "newMessage", null);

        adminRequest(performJson(put("/message"), message)).andExpect(status().isOk());

        message.setCreatedBy(new Action(new User(100L, null, null, null)));
        long id = getIdByColumnAndValue("text", message.getText());

        expectMessage(adminRequest(get(String.format("/message/%d", id))), message);
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_comments.sql")
    @Sql("/scripts/insert_messages.sql")
    void updateChangeComment() throws Exception {
        long commentId = 500;
        Message message = new Message(600L, commentId + 1, "newMessage", null);

        adminRequest(performJson(put("/message"), message)).andExpect(status().isOk());

        message.setCreatedBy(new Action(new User(100L, null, null, null)));
        message.setCommentId(commentId);
        long id = getIdByColumnAndValue("text", message.getText());

        expectMessage(adminRequest(get(String.format("/message/%d", id))), message);
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_comments.sql")
    @Sql("/scripts/insert_messages.sql")
    void updateEmptyText() throws Exception {
        long commentId = 500;
        Message message = new Message(600L, commentId, "", null);

        adminRequest(performJson(put("/message"), message)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_comments.sql")
    @Sql("/scripts/insert_messages.sql")
    void updateNullText() throws Exception {
        long commentId = 500;
        Message message = new Message(600L, commentId, null, null);

        adminRequest(performJson(put("/message"), message)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_comments.sql")
    @Sql("/scripts/insert_messages.sql")
    void updateResolvedComment() throws Exception {
        long commentId = 502;
        Message message = new Message(601L, commentId, "newMessage", null);

        adminRequest(performJson(put("/message"), message)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_comments.sql")
    @Sql("/scripts/insert_messages.sql")
    void deleteById() throws Exception {
        adminRequest(delete("/message/600")).andExpect(status().isOk());

        assertNotFound(600);
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_comments.sql")
    @Sql("/scripts/insert_messages.sql")
    void deleteByIdNotFound() throws Exception {
        adminRequest(delete("/message/610")).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_comments.sql")
    @Sql("/scripts/insert_messages.sql")
    void deleteByIdTwice() throws Exception {
        adminRequest(delete("/message/600")).andExpect(status().isOk());

        assertNotFound(600);

        adminRequest(delete("/message/600")).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_comments.sql")
    @Sql("/scripts/insert_messages.sql")
    void getById() throws Exception {
        long commentId = 500;
        long id = 600;

        Message message = new Message(id, commentId, "message600", new Action(new User(100L, null, null, null)));

        expectMessage(adminRequest(get(String.format("/message/%d", id))), message);
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_comments.sql")
    @Sql("/scripts/insert_messages.sql")
    void getByIdNotFound() throws Exception {
        long id = 610;

        adminRequest(get(String.format("/message/%d", id))).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_comments.sql")
    @Sql("/scripts/insert_messages.sql")
    void getByCommentId() throws Exception {
        Action action = new Action(new User(100L, null, null, null));

        Message message600 = new Message(600L, 500L, "message600", action);
        Message message602 = new Message(602L, 500L, "message602", action);

        String content = adminRequest(get("/message/comment/500")).andReturn().getResponse().getContentAsString();
        List<Message> actualMessages = objectMapper.readValue(content, new TypeReference<>() {
        });
        actualMessages.forEach(card -> {
            long userId = card.getCreatedBy().getAuthor().getId();
            card.setCreatedBy(new Action(new User(userId, null, null, null)));
        });

        Assertions.assertEquals(List.of(message600, message602), actualMessages);
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_comments.sql")
    @Sql("/scripts/insert_messages.sql")
    void getEmptyListByCommentId() throws Exception {
        adminRequest(get("/message/comment/501"))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(Collections.emptyList())));
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_comments.sql")
    @Sql("/scripts/insert_messages.sql")
    void getByNonExistentComment() throws Exception {
        adminRequest(get("/message/comment/510"))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(Collections.emptyList())));
    }

    protected void expectMessage(ResultActions resultActions, Message expectedMessage) throws Exception {
        resultActions.andExpect(status().isOk());

        String content = resultActions.andReturn().getResponse().getContentAsString();
        Message actualMessage = objectMapper.readValue(content, Message.class);

        assertEquals(expectedMessage, actualMessage);
    }

    private void assertEquals(Message expectedMessage, Message actualMessage) {
        Assertions.assertEquals(expectedMessage.getText(), actualMessage.getText());
        Assertions.assertEquals(expectedMessage.getCommentId(), actualMessage.getCommentId());
        Assertions.assertEquals(expectedMessage.getCreatedBy().getAuthor().getId(),
                                actualMessage.getCreatedBy().getAuthor().getId());
    }

}
