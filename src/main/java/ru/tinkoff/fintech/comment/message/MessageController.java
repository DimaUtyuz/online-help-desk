package ru.tinkoff.fintech.comment.message;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/message")
@AllArgsConstructor
public class MessageController {

    private final MessageService messageService;

    @PostMapping
    public void save(@RequestBody @Valid Message message) {
        messageService.add(message);
    }

    @GetMapping("/{id}")
    public Message getById(@PathVariable("id") Long id) {
        return messageService.getById(id);
    }

    @GetMapping("/comment/{commentId}")
    public List<Message> getByCommentId(@PathVariable("commentId") Long commentId) {
        return messageService.getByCommentId(commentId);
    }

    @PutMapping
    public void update(@RequestBody @Valid Message message) {
        messageService.update(message);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") Long id) {
        messageService.delete(id);
    }

}
