package ru.tinkoff.fintech.tag;

import java.util.List;

public interface Tagged {

    List<Tag> getTagList();

}
