package ru.tinkoff.fintech.comment.message;

import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Optional;

@Mapper
public interface MessageRepository {

    @Insert("""
            insert into messages (comment_id, text, created_id)
                values (#{commentId}, #{text}, #{createdBy.id})
            """)
    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
    void save(Message message);

    @Select("""
            select m.id, m.comment_id, m.text, m.created_id
                from messages as m
            where m.id = #{id}
            """)
    @Results(id = "message", value = {
            @Result(property = "id", column = "id"),
            @Result(property = "commentId", column = "comment_id"),
            @Result(property = "text", column = "text"),
            @Result(property = "createdBy", column = "created_id",
                    one = @One(select = "ru.tinkoff.fintech.action.ActionRepository.findById"))
    })
    Optional<Message> findById(Long id);

    @Select("""
            select m.id, m.comment_id, m.text, m.created_id
                from messages as m
            where m.comment_id = #{id}
            """)
    @ResultMap("message")
    List<Message> findMessagesByCommentId(Long id);

    @Update("""
            update messages
                set text = #{text}
            where id = #{id}
            """)
    Long update(Message message);

    @Delete("""
            delete
                from messages
            where id = #{id}
            """)
    Long delete(Long id);

}
