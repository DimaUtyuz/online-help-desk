package ru.tinkoff.fintech.kanban.card;

import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Optional;

@Mapper
public interface CardRepository {

    @Insert("""
            insert into cards (name, description, created_id, modified_id, kanban_id, status_id, assignee_id)
                values (#{name}, #{description}, #{createdBy.id}, #{lastModifiedBy.id}, #{kanbanId},
                        #{status.id}, #{assignee.id})
            """)
    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
    void save(Card card);

    @Select("""
            select c.id, c.name, c.description, c.created_id, c.modified_id, c.status_id, c.assignee_id, c.kanban_id
                from cards as c
            where c.id = #{id}
            """)
    @Results(id = "card", value = {
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "kanbanId", column = "kanban_id"),
            @Result(property = "createdBy", column = "created_id",
                    one = @One(select = "ru.tinkoff.fintech.action.ActionRepository.findById")),
            @Result(property = "lastModifiedBy", column = "modified_id",
                    one = @One(select = "ru.tinkoff.fintech.action.ActionRepository.findById")),
            @Result(property = "status", column = "status_id",
                    one = @One(select = "ru.tinkoff.fintech.kanban.status.StatusRepository.findById")),
            @Result(property = "assignee", column = "assignee_id",
                    one = @One(select = "ru.tinkoff.fintech.security.user.UserRepository.findById")),
            @Result(property = "tagList", column = "id",
                    many = @Many(select = "ru.tinkoff.fintech.tag.list.BlockTagsRepository.findTagsByBlockId"))
    })
    Optional<Card> findById(Long id);

    @Select("""
            select c.id, c.name, c.description, c.created_id, c.modified_id, c.status_id, c.assignee_id, c.kanban_id
                from cards as c
            """)
    @ResultMap("card")
    List<Card> findAll();

    @Select("""
            select c.id, c.name, c.description, c.created_id, c.modified_id, c.status_id, c.assignee_id, c.kanban_id
                from cards as c
            where c.kanban_id = #{kanbanId}
            """)
    @ResultMap("card")
    List<Card> findByKanbanId(Long kanbanId);

    @Update("""
            update cards
                set name = #{name}, description = #{description}, modified_id = #{lastModifiedBy.id},
                    assignee_id = #{assignee.id}, status_id = #{status.id}, kanban_id = #{kanbanId}
            where id = #{id}
            """)
    Long update(Card card);

    @Delete("""
            delete
                from cards
            where id = #{id}
            """)
    Long delete(Long id);

}
