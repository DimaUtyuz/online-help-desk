package ru.tinkoff.fintech.comment;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tinkoff.fintech.action.aop.annotation.CreateBlockBy;
import ru.tinkoff.fintech.action.aop.annotation.ModifyBlockBy;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
@AllArgsConstructor
public class CommentService {

    private final CommentRepository commentRepository;

    @Transactional
    @CreateBlockBy
    public void add(Comment comment) {
        commentRepository.save(comment);
    }

    public Comment getById(Long id) {
        return commentRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Comment with id: " + id + " not found"));
    }

    public List<Comment> getAll() {
        return commentRepository.findAll();
    }

    @Transactional
    @ModifyBlockBy
    public void update(Comment comment) {
        if (commentRepository.update(comment) == 0) {
            throw new EntityNotFoundException("Comment with id: " + comment.getId() + " not found");
        }
    }

    @Transactional
    public void delete(Long id) {
        if (commentRepository.delete(id) == 0) {
            throw new EntityNotFoundException("Comment with id: " + id + " not found");
        }
    }

}
