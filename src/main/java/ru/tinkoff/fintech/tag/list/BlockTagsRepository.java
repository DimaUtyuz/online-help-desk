package ru.tinkoff.fintech.tag.list;

import org.apache.ibatis.annotations.*;
import ru.tinkoff.fintech.block.Block;
import ru.tinkoff.fintech.table.Table;
import ru.tinkoff.fintech.tag.Tag;

import java.util.List;
import java.util.Optional;

@Mapper
public interface BlockTagsRepository {

    @Insert("""
            insert into tag_block (tag_id, block_id)
                values (#{tagId}, #{blockId})
            """)
    void save(Long tagId, Long blockId);

    @Delete("""
            delete
                from tag_block
            where tag_id = #{tagId}
                and block_id = #{blockId}
            """)
    void delete(Long tagId, Long blockId);

    @Delete("""
            delete
                from tag_block
            where block_id = #{id}
            """)
    void deleteBlock(Long id);

    @Select("""
            select t.id, t.name
                from tag_block as tb
            join tags as t on tb.tag_id = t.id
            where tb.block_id = #{id}
            """)
    @Results(id = "tag", value = {
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name")
    })
    List<Tag> findTagsByBlockId(Long id);

    @Select("""
            select tb.block_id
                from tag_block as tb
            where tb.tag_id = #{id}
            """)
    List<Long> findBlockIdsByTagId(Long id);

}
