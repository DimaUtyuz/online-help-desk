package ru.tinkoff.fintech.action.aop;

import lombok.AllArgsConstructor;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;
import ru.tinkoff.fintech.action.Action;
import ru.tinkoff.fintech.action.ActionService;
import ru.tinkoff.fintech.block.Block;
import ru.tinkoff.fintech.comment.message.Message;

@Aspect
@Component
@AllArgsConstructor
public class ActionAspect {

    private ActionService actionService;

    @Before("@annotation(ru.tinkoff.fintech.action.aop.annotation.CreateBlockBy) && args(block,..)")
    public void save(Block block) {
        Action action = actionService.createAction();
        block.setCreatedBy(action);
        block.setLastModifiedBy(action);
    }

    @Before("@annotation(ru.tinkoff.fintech.action.aop.annotation.ModifyBlockBy) && args(block,..)")
    public void update(Block block) {
        Action action = actionService.createAction();
        block.setLastModifiedBy(action);
    }

    @Before("@annotation(ru.tinkoff.fintech.action.aop.annotation.CreateMessageBy) && args(message,..)")
    public void save(Message message) {
        Action action = actionService.createAction();
        message.setCreatedBy(action);
    }

}
