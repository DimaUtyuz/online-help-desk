package ru.tinkoff.fintech.kanban.status.list;

import lombok.AllArgsConstructor;
import lombok.Data;
import ru.tinkoff.fintech.kanban.Kanban;
import ru.tinkoff.fintech.kanban.status.Status;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@AllArgsConstructor
public class KanbanStatusesList {

    @NotNull(message = "Набор статусов не задан.")
    private List<Status> statusList;

    @NotNull(message = "Доска не определена.")
    private Long kanbanId;

    public KanbanStatusesList(Kanban kanban) {
        statusList = kanban.getStatusList();
        kanbanId = kanban.getId();
    }

}
