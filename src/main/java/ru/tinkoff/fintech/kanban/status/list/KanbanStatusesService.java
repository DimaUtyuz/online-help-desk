package ru.tinkoff.fintech.kanban.status.list;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tinkoff.fintech.kanban.status.Status;
import ru.tinkoff.fintech.kanban.status.StatusService;

import java.util.List;

@Service
@AllArgsConstructor
public class KanbanStatusesService {

    private final KanbanStatusesRepository kanbanStatusesRepository;
    private final StatusService statusService;

    @Transactional
    public void add(KanbanStatusesList kanbanStatusesList) {
        List<Status> statusList = kanbanStatusesList.getStatusList();
        statusList.replaceAll(status -> statusService.getOrAdd(status.getName()));

        Long kanbanId = kanbanStatusesList.getKanbanId();
        kanbanStatusesRepository.deleteKanban(kanbanId);
        for (Status status : statusList) {
            kanbanStatusesRepository.save(status.getId(), kanbanId);
        }
    }

    public void delete(Long statusId, Long kanbanId) {
        kanbanStatusesRepository.delete(statusId, kanbanId);
    }

    public boolean isStatusExist(Long statusId, Long kanbanId) {
        return kanbanStatusesRepository.checkStatusExists(statusId, kanbanId);
    }

}
