package ru.tinkoff.fintech.table.cell;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.ResultActions;
import ru.tinkoff.fintech.ControllerTest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class CellControllerTest extends ControllerTest<Cell> {

    static {
        tableName = "cells";
        entityClass = Cell.class;
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_tables.sql")
    @Sql("/scripts/insert_cells.sql")
    void update() throws Exception {
        long id = 800;
        Cell cell = new Cell(id, 1, 1, "newText");
        adminRequest(performJson(put("/cell"), cell)).andExpect(status().isOk());

        expectCell(adminRequest(get(String.format("/cell/%d", id))), cell);
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_tables.sql")
    @Sql("/scripts/insert_cells.sql")
    void updateEmptyText() throws Exception {
        long id = 800;
        Cell cell = new Cell(id, 1, 1, "");
        adminRequest(performJson(put("/cell"), cell)).andExpect(status().isOk());

        expectCell(adminRequest(get(String.format("/cell/%d", id))), cell);
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_tables.sql")
    @Sql("/scripts/insert_cells.sql")
    void updateNullText() throws Exception {
        long id = 800;
        Cell cell = new Cell(id, 1, 1, null);
        adminRequest(performJson(put("/cell"), cell)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_tables.sql")
    @Sql("/scripts/insert_cells.sql")
    void updateNonExistentCell() throws Exception {
        long id = 810;
        Cell cell = new Cell(id, 1, 1, "text");
        adminRequest(performJson(put("/cell"), cell)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_tables.sql")
    @Sql("/scripts/insert_cells.sql")
    void updateIgnoreChange() throws Exception {
        long id = 800;
        Cell cell = new Cell(id, 1, 1, "newText");
        Cell changedCell = new Cell(id, 2, 2, "newText");
        adminRequest(performJson(put("/cell"), changedCell)).andExpect(status().isOk());

        expectCell(adminRequest(get(String.format("/cell/%d", id))), cell);
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_tables.sql")
    @Sql("/scripts/insert_cells.sql")
    void getById() throws Exception {
        long id = 800;
        Cell cell = new Cell(id, 1, 1, "text11");

        expectCell(adminRequest(get(String.format("/cell/%d", id))), cell);
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_tables.sql")
    @Sql("/scripts/insert_cells.sql")
    void getByIdNotFound() throws Exception {
        long id = 810;
        adminRequest(get(String.format("/cell/%d", id))).andExpect(status().is4xxClientError());
    }

    protected void expectCell(ResultActions resultActions, Cell expectedCell) throws Exception {
        resultActions.andExpect(status().isOk());

        String content = resultActions.andReturn().getResponse().getContentAsString();
        Cell actualCell = objectMapper.readValue(content, Cell.class);

        assertEquals(expectedCell, actualCell);
    }

    private void assertEquals(Cell expectedCell, Cell actualCell) {
        Assertions.assertEquals(expectedCell.getText(), actualCell.getText());
        Assertions.assertEquals(expectedCell.getColumn(), actualCell.getColumn());
        Assertions.assertEquals(expectedCell.getRow(), actualCell.getRow());
    }

}
