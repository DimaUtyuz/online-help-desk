package ru.tinkoff.fintech.security.user;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/user")
@AllArgsConstructor
public class UserController {

    private final UserService userService;

    @PostMapping
    public void save(@RequestBody @Valid User user) {
        userService.add(user);
    }

    @GetMapping("/{login}")
    public User getByLogin(@PathVariable("login") String login) {
        return userService.getByLogin(login);
    }

}
