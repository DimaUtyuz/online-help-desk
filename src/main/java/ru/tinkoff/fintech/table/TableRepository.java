package ru.tinkoff.fintech.table;

import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Optional;

@Mapper
public interface TableRepository {

    @Insert("""
            insert into tables (name, description, created_id, modified_id, board_id, height, width)
                values (#{name}, #{description}, #{createdBy.id}, #{lastModifiedBy.id},
                        #{boardId}, #{height}, #{width})
            """)
    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
    void save(Table table);

    @Select("""
            select t.id, t.name, t.description, t.created_id, t.modified_id, t.height, t.width, t.board_id
                from tables as t
            where t.id = #{id}
            """)
    @Results(id = "table", value = {
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "boardId", column = "board_id"),
            @Result(property = "height", column = "height"),
            @Result(property = "width", column = "width"),
            @Result(property = "createdBy", column = "created_id",
                    one = @One(select = "ru.tinkoff.fintech.action.ActionRepository.findById")),
            @Result(property = "lastModifiedBy", column = "modified_id",
                    one = @One(select = "ru.tinkoff.fintech.action.ActionRepository.findById")),
            @Result(property = "cells", column = "id",
                    many = @Many(select = "ru.tinkoff.fintech.table.cell.CellRepository.findByTableId"))
    })
    Optional<Table> findById(Long id);

    @Select("""
            select t.id, t.name, t.description, t.created_id, t.modified_id, t.height, t.width, t.board_id
                from tables as t
            """)
    @ResultMap("table")
    List<Table> findAll();

    @Select("""
            select t.id, t.name, t.description, t.created_id, t.modified_id, t.height, t.width, t.board_id
                from tables as t
            where t.board_id = #{boardId}
            """)
    @ResultMap("table")
    List<Table> findByBoardId(Long boardId);

    @Update("""
            update tables
                set name = #{name}, description = #{description}, modified_id = #{lastModifiedBy.id}
            where id = #{id}
            """)
    Long update(Table table);

    @Update("""
            update tables
                set height = #{height}, width = #{width}, modified_id = #{lastModifiedBy.id}
            where id = #{id}
            """)
    Long updateSize(Table table);

    @Delete("""
            delete
                from tables
            where id = #{id}
            """)
    Long delete(Long id);

    @Select("""
            select t.id
                from tables as t
            where t.board_id = #{boardId}
            """)
    List<Long> findIdsByBoardId(Long boardId);

}
