CREATE TABLE roles
(
    id   BIGSERIAL    PRIMARY KEY,
    name VARCHAR(64)  NOT NULL
);

INSERT INTO roles (id, name) VALUES (1, 'ADMIN');
INSERT INTO roles (id, name) VALUES (2, 'USER');

CREATE TABLE users
(
    id        BIGSERIAL    PRIMARY KEY,
    login     VARCHAR(255) UNIQUE NOT NULL,
    password  VARCHAR(255) NOT NULL,
    role_id   BIGINT       NOT NULL,
    FOREIGN KEY (role_id)  REFERENCES roles(id)
);

INSERT INTO users (id, login, password, role_id)
VALUES (100, 'admin', '$2a$10$i2FpNeJQJiw9ziVUStOD/eDO4DyWF8Yz8arny3M8cyjG6sbiTDFZ2', 1);
INSERT INTO users (id, login, password, role_id)
VALUES (200, 'user', '$2a$10$S/jl3cuwIdHyR.Nd6gbNYu/LLtFMIAgy/mLYQxekSFvN7vCCXdEBy', 2);

CREATE TABLE actions
(
    id            BIGSERIAL PRIMARY KEY,
    user_id       BIGINT    NOT NULL,
    creation_time TIMESTAMP NOT NULL,
    FOREIGN KEY (user_id) REFERENCES users(id)
);

CREATE TABLE blocks
(
    id           BIGSERIAL     PRIMARY KEY,
    name         VARCHAR(256)  NOT NULL,
    description  VARCHAR(4096) NOT NULL,
    created_id   BIGINT        NOT NULL,
    modified_id  BIGINT        NOT NULL,
    FOREIGN KEY (created_id)  REFERENCES actions(id),
    FOREIGN KEY (modified_id) REFERENCES actions(id)
);

CREATE TABLE boards
(
) INHERITS (blocks);

ALTER TABLE boards ADD PRIMARY KEY (id);

CREATE TABLE forms
(
    board_id BIGINT NOT NULL
) INHERITS (blocks);

CREATE TABLE comments
(
    resolved  BOOLEAN   NOT NULL
) INHERITS (forms);

ALTER TABLE comments ADD FOREIGN KEY (board_id) REFERENCES boards(id);
ALTER TABLE comments ADD PRIMARY KEY (id);

CREATE TABLE messages
(
    id          BIGSERIAL     PRIMARY KEY,
    comment_id  BIGINT        NOT NULL,
    text        VARCHAR(4096) NOT NULL,
    created_id  BIGINT        NOT NULL,
    FOREIGN KEY (comment_id) REFERENCES comments(id),
    FOREIGN KEY (created_id) REFERENCES actions(id)
);

CREATE TABLE tables
(
    height INT NOT NULL,
    width  INT NOT NULL
) INHERITS (forms);

ALTER TABLE tables ADD FOREIGN KEY (board_id) REFERENCES boards(id);
ALTER TABLE tables ADD PRIMARY KEY (id);

CREATE TABLE cells
(
    id        BIGSERIAL      PRIMARY KEY,
    table_id  BIGINT         NOT NULL,
    "column"  INTEGER        NOT NULL,
    "row"     INTEGER        NOT NULL,
    "text"    VARCHAR(4096)  NOT NULL,
    FOREIGN KEY (table_id) REFERENCES tables(id)
);

CREATE TABLE statuses
(
    id    BIGSERIAL   PRIMARY KEY,
    name  VARCHAR(64) NOT NULL
);

CREATE TABLE kanbans
(
) INHERITS (forms);

ALTER TABLE kanbans ADD FOREIGN KEY (board_id) REFERENCES boards(id);
ALTER TABLE kanbans ADD PRIMARY KEY (id);

CREATE TABLE cards
(
    kanban_id    BIGINT NOT NULL,
    status_id    BIGINT NOT NULL,
    assignee_id  BIGINT NOT NULL,
    FOREIGN KEY (kanban_id)   REFERENCES kanbans(id),
    FOREIGN KEY (status_id)   REFERENCES statuses(id),
    FOREIGN KEY (assignee_id) REFERENCES users(id)
) INHERITS (blocks);

ALTER TABLE cards ADD PRIMARY KEY (id);

CREATE TABLE status_kanban
(
    status_id  BIGINT NOT NULL,
    kanban_id  BIGINT NOT NULL,
    PRIMARY KEY (status_id, kanban_id),
    FOREIGN KEY (status_id)  REFERENCES statuses(id),
    FOREIGN KEY (kanban_id)  REFERENCES kanbans(id)
);

CREATE TABLE notes
(
) INHERITS (forms);

ALTER TABLE notes ADD FOREIGN KEY (board_id) REFERENCES boards(id);
ALTER TABLE notes ADD PRIMARY KEY (id);

CREATE TABLE note_to_note
(
    from_note_id BIGINT NOT NULL,
    to_note_id   BIGINT NOT NULL,
    PRIMARY KEY (from_note_id, to_note_id),
    FOREIGN KEY (from_note_id) REFERENCES notes(id),
    FOREIGN KEY (to_note_id)   REFERENCES notes(id)
);

CREATE TABLE tags
(
    id   BIGSERIAL   PRIMARY KEY,
    name VARCHAR(64) NOT NULL
);

CREATE TABLE tag_block
(
    tag_id  BIGINT NOT NULL,
    block_id BIGINT NOT NULL,
    PRIMARY KEY (tag_id, block_id),
    FOREIGN KEY (tag_id) REFERENCES tags(id)
);
