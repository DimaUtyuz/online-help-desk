package ru.tinkoff.fintech.tag;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class TagService {

    private final TagRepository tagRepository;

    public void add(Tag tag) {
        tagRepository.save(tag);
    }

    public Tag getById(Long id) {
        return tagRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Tag with id: " + id + " not found"));
    }

    @Transactional
    public Tag getOrAdd(String name) {
        isValidTagName(name);

        Optional<Tag> tagOptional = tagRepository.findByName(name);
        if (tagOptional.isPresent()) {
            return tagOptional.get();
        } else {
            Tag tag = new Tag(name);
            add(tag);
            return tag;
        }
    }

    public Tag getByName(String name) {
        return tagRepository.findByName(name)
                .orElseThrow(() -> new EntityNotFoundException("Tag with name: " + name + " not found"));
    }

    public List<Tag> getAll() {
        return tagRepository.findAll();
    }

    private void isValidTagName(String name) {
        if (name == null) {
            throw new IllegalArgumentException("Tag name can not be null or blank");
        }
        if (name.isBlank()) {
            throw new IllegalArgumentException("Tag name can not be blank");
        }
    }

}
