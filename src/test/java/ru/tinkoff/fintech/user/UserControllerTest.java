package ru.tinkoff.fintech.user;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.ResultActions;
import ru.tinkoff.fintech.ControllerTest;
import ru.tinkoff.fintech.security.role.Role;
import ru.tinkoff.fintech.security.user.User;
import ru.tinkoff.fintech.security.user.UserService;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class UserControllerTest extends ControllerTest<User> {

    @Autowired
    private UserService userService;

    @Autowired
    private PasswordEncoder bCryptPasswordEncoder;

    static {
        tableName = "users";
        entityClass = User.class;
    }

    @Test
    void saveByAdmin() throws Exception {
        User expectedUser = new User("test", "test");

        adminRequest(performJson(post("/user"), expectedUser)).andExpect(status().isOk());

        User actualUser = userService.getByLogin("test");

        Assertions.assertNotNull(actualUser);
        Assertions.assertNotNull(actualUser.getPassword());
        Assertions.assertNotNull(actualUser.getLogin());
        Assertions.assertNotNull(actualUser.getRole());

        Assertions.assertTrue(bCryptPasswordEncoder.matches(expectedUser.getPassword(), actualUser.getPassword()));

        Assertions.assertEquals(expectedUser.getLogin(), actualUser.getLogin());

        Assertions.assertEquals(new Role(2L, "USER"), actualUser.getRole());
    }

    @Test
    void saveByUser() throws Exception {
        User expectedUser = new User("test", "test");

        userRequest(performJson(post("/user"), expectedUser)).andExpect(status().isForbidden());
    }

    @Test
    void saveByNonExistentUser() throws Exception {
        User expectedUser = new User("test", "test");

        nonExistentUserRequest(performJson(post("/user"), expectedUser)).andExpect(status().isUnauthorized());
    }

    @Test
    @Sql("/scripts/insert_users.sql")
    void getAdminByLogin() throws Exception {
        User user = new User(1L, "admin", "admin", new Role(1L, "ADMIN"));

        expectUser(adminRequest(get("/user/admin")), user);
    }

    @Test
    @Sql("/scripts/insert_users.sql")
    void getByLoginByAdmin() throws Exception {
        User user = new User(1L, "user", "user", new Role(2L, "USER"));

        expectUser(adminRequest(get("/user/user")), user);
    }

    @Test
    @Sql("/scripts/insert_users.sql")
    void getByLoginByUser() throws Exception {
        User user = new User(1L, "user", "user", new Role(2L, "USER"));

        expectUser(userRequest(get("/user/user")), user);
    }

    @Test
    @Sql("/scripts/insert_users.sql")
    void getByLoginByNonExistentUser() throws Exception {
        nonExistentUserRequest(get("/user/user")).andExpect(status().isUnauthorized());
    }

    protected void expectUser(ResultActions resultActions, User user) throws Exception {
        resultActions
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.login").value(user.getLogin()))
                .andExpect(jsonPath("$.role").value(user.getRole()));
    }

}
