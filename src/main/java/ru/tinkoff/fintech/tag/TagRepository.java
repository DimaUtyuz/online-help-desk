package ru.tinkoff.fintech.tag;

import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Optional;

@Mapper
public interface TagRepository {

    @Insert("insert into tags (name) values (#{name})")
    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
    void save(Tag tag);

    @Select("""
            select t.id, t.name
                from tags as t
            where t.id = #{id}
            """)
    @Results(id = "tag", value = {
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name")
    })
    Optional<Tag> findById(Long id);

    @Select("""
            select t.id, t.name
                from tags as t
            where t.name = #{name}
            """)
    @ResultMap("tag")
    Optional<Tag> findByName(String name);

    @Select("""
            select t.id, t.name
                from tags as t
            """)
    @ResultMap("tag")
    List<Tag> findAll();

}
