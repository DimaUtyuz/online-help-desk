package ru.tinkoff.fintech.board;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/board")
@AllArgsConstructor
public class BoardController {

    private final BoardService boardService;

    @PostMapping
    public void save(@RequestBody @Valid Board board) {
        boardService.add(board);
    }

    @GetMapping("/{id}")
    public Board getById(@PathVariable("id") Long id) {
        return boardService.getById(id);
    }

    @GetMapping
    public List<Board> getAll() {
        return boardService.getAll();
    }

    @PutMapping
    public void update(@RequestBody @Valid Board board) {
        boardService.update(board);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") Long id) {
        boardService.delete(id);
    }

}
