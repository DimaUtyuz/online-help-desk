package ru.tinkoff.fintech.table.cell;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Cell {

    private Long id;

    private int column;

    private int row;

    @NotNull(message = "Текст не определён.")
    private String text;

    public Cell(int column, int row, String text) {
        this.column = column;
        this.row = row;
        this.text = text;
    }
}
