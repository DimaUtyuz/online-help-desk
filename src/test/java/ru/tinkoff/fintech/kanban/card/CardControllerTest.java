package ru.tinkoff.fintech.kanban.card;

import com.fasterxml.jackson.core.type.TypeReference;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.ResultActions;
import ru.tinkoff.fintech.ControllerTest;
import ru.tinkoff.fintech.kanban.status.Status;
import ru.tinkoff.fintech.security.user.User;
import ru.tinkoff.fintech.tag.Tag;

import java.util.Collections;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class CardControllerTest extends ControllerTest<Card> {

    static {
        tableName = "cards";
        entityClass = Card.class;
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_statuses.sql")
    @Sql("/scripts/insert_kanbans.sql")
    void saveWithoutTags() throws Exception {
        long kanbanId = 300;
        Card card = new Card(kanbanId,
                "card400",
                "d400",
                new Status(100L, "status100"),
                new User(100L, null, null, null),
                Collections.emptyList()
        );

        adminRequest(performJson(post("/card"), card)).andExpect(status().isOk());

        long id = getIdByName(card.getName());

        expectCard(adminRequest(get(String.format("/card/%d", id))), card);
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_statuses.sql")
    @Sql("/scripts/insert_kanbans.sql")
    void saveWithTags() throws Exception {
        long kanbanId = 300;
        Card card = new Card(kanbanId,
                "card400",
                "d400",
                new Status(100L, "status100"),
                new User(100L, null, null, null),
                List.of(new Tag("tag1"), new Tag("tag2"))
        );

        adminRequest(performJson(post("/card"), card)).andExpect(status().isOk());

        long id = getIdByName(card.getName());

        expectCard(adminRequest(get(String.format("/card/%d", id))), card);
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_statuses.sql")
    @Sql("/scripts/insert_kanbans.sql")
    void saveNonExistentKanban() throws Exception {
        long kanbanId = 305;
        Card card = new Card(kanbanId,
                "card400",
                "d400",
                new Status(100L, "status100"),
                new User(100L, null, null, null),
                List.of(new Tag("tag1"), new Tag("tag2"))
        );

        adminRequest(performJson(post("/card"), card)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_statuses.sql")
    @Sql("/scripts/insert_kanbans.sql")
    void saveEmptyName() throws Exception {
        long kanbanId = 300;
        Card card = new Card(kanbanId,
                "",
                "d400",
                new Status(100L, "status100"),
                new User(100L, null, null, null),
                Collections.emptyList()
        );

        adminRequest(performJson(post("/card"), card)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_statuses.sql")
    @Sql("/scripts/insert_kanbans.sql")
    void saveNullName() throws Exception {
        long kanbanId = 300;
        Card card = new Card(kanbanId,
                null,
                "d400",
                new Status(100L, "status100"),
                new User(100L, null, null, null),
                Collections.emptyList()
        );

        adminRequest(performJson(post("/card"), card)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_statuses.sql")
    @Sql("/scripts/insert_kanbans.sql")
    void saveEmptyDescription() throws Exception {
        long kanbanId = 300;
        Card card = new Card(kanbanId,
                "card400",
                "",
                new Status(100L, "status100"),
                new User(100L, null, null, null),
                Collections.emptyList()
        );

        adminRequest(performJson(post("/card"), card)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_statuses.sql")
    @Sql("/scripts/insert_kanbans.sql")
    void saveNullDescription() throws Exception {
        long kanbanId = 300;
        Card card = new Card(kanbanId,
                "card400",
                null,
                new Status(100L, "status100"),
                new User(100L, null, null, null),
                Collections.emptyList()
        );

        adminRequest(performJson(post("/card"), card)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_statuses.sql")
    @Sql("/scripts/insert_kanbans.sql")
    void saveNullStatus() throws Exception {
        long kanbanId = 300;
        Card card = new Card(kanbanId,
                "card400",
                "d400",
                null,
                new User(100L, null, null, null),
                Collections.emptyList()
        );

        adminRequest(performJson(post("/card"), card)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_statuses.sql")
    @Sql("/scripts/insert_kanbans.sql")
    void saveNotAvailableStatus() throws Exception {
        long kanbanId = 300;
        Card card = new Card(kanbanId,
                "card400",
                "d400",
                new Status(200L, "status200"),
                new User(100L, null, null, null),
                Collections.emptyList()
        );

        adminRequest(performJson(post("/card"), card)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_statuses.sql")
    @Sql("/scripts/insert_kanbans.sql")
    void saveNullStatusId() throws Exception {
        long kanbanId = 300;
        Card card = new Card(kanbanId,
                "card400",
                "d400",
                new Status(null, "status200"),
                new User(100L, null, null, null),
                Collections.emptyList()
        );

        adminRequest(performJson(post("/card"), card)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_statuses.sql")
    @Sql("/scripts/insert_kanbans.sql")
    void saveNullAssignee() throws Exception {
        long kanbanId = 300;
        Card card = new Card(kanbanId,
                "card400",
                "d400",
                new Status(100L, "status100"),
                null,
                Collections.emptyList()
        );

        adminRequest(performJson(post("/card"), card)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_statuses.sql")
    @Sql("/scripts/insert_kanbans.sql")
    void saveNullAssigneeId() throws Exception {
        long kanbanId = 300;
        Card card = new Card(kanbanId,
                "card400",
                "d400",
                new Status(100L, "status100"),
                new User(null, null, null, null),
                Collections.emptyList()
        );

        adminRequest(performJson(post("/card"), card)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_statuses.sql")
    @Sql("/scripts/insert_kanbans.sql")
    void saveNotExistentAssignee() throws Exception {
        long kanbanId = 300;
        Card card = new Card(kanbanId,
                "card400",
                "d400",
                new Status(100L, "status100"),
                new User(300L, null, null, null),
                Collections.emptyList()
        );

        adminRequest(performJson(post("/card"), card)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_statuses.sql")
    @Sql("/scripts/insert_kanbans.sql")
    void saveNullTagList() throws Exception {
        long kanbanId = 300;
        Card card = new Card(kanbanId,
                "card400",
                "d400",
                new Status(100L, "status100"),
                new User(100L, null, null, null),
                null
        );

        adminRequest(performJson(post("/card"), card)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_statuses.sql")
    @Sql("/scripts/insert_kanbans.sql")
    void saveEmptyTagName() throws Exception {
        long kanbanId = 300;
        Card card = new Card(kanbanId,
                "card400",
                "d400",
                new Status(100L, "status100"),
                new User(100L, null, null, null),
                List.of(new Tag(""))
        );

        adminRequest(performJson(post("/card"), card)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_statuses.sql")
    @Sql("/scripts/insert_kanbans.sql")
    void saveNullTagName() throws Exception {
        long kanbanId = 300;
        Card card = new Card(kanbanId,
                "card400",
                "d400",
                new Status(100L, "status100"),
                new User(100L, null, null, null),
                List.of(new Tag(null))
        );

        adminRequest(performJson(post("/card"), card)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_statuses.sql")
    @Sql("/scripts/insert_tags.sql")
    @Sql("/scripts/insert_kanbans.sql")
    @Sql("/scripts/insert_cards.sql")
    void update() throws Exception {
        long kanbanId = 300;
        Card card = new Card(kanbanId,
                "newCard",
                "newD",
                new Status(100L, "status100"),
                new User(200L, null, null, null),
                List.of(new Tag("tag2"))
        );
        long id = 400L;
        card.setId(id);

        adminRequest(performJson(put("/card"), card)).andExpect(status().isOk());

        expectCard(adminRequest(get(String.format("/card/%d", id))), card);
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_statuses.sql")
    @Sql("/scripts/insert_tags.sql")
    @Sql("/scripts/insert_kanbans.sql")
    @Sql("/scripts/insert_cards.sql")
    void updateNotExistentCard() throws Exception {
        long kanbanId = 300;
        Card card = new Card(kanbanId,
                "newCard",
                "newD",
                new Status(100L, "status100"),
                new User(200L, null, null, null),
                List.of(new Tag("tag2"))
        );
        long id = 405L;
        card.setId(id);

        adminRequest(performJson(put("/card"), card)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_statuses.sql")
    @Sql("/scripts/insert_tags.sql")
    @Sql("/scripts/insert_kanbans.sql")
    @Sql("/scripts/insert_cards.sql")
    void updateWithNewStatus() throws Exception {
        long kanbanId = 300;
        Card card = new Card(kanbanId,
                "newCard",
                "newD",
                new Status(101L, "status101"),
                new User(200L, null, null, null),
                List.of(new Tag("tag2"))
        );
        long id = 400L;
        card.setId(id);

        adminRequest(performJson(put("/card"), card)).andExpect(status().isOk());

        expectCard(adminRequest(get(String.format("/card/%d", id))), card);
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_statuses.sql")
    @Sql("/scripts/insert_tags.sql")
    @Sql("/scripts/insert_kanbans.sql")
    @Sql("/scripts/insert_cards.sql")
    void updateEmptyName() throws Exception {
        long kanbanId = 300;
        Card card = new Card(kanbanId,
                "",
                "newD",
                new Status(100L, "status100"),
                new User(200L, null, null, null),
                List.of(new Tag("tag2"))
        );
        long id = 400L;
        card.setId(id);

        adminRequest(performJson(put("/card"), card)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_statuses.sql")
    @Sql("/scripts/insert_tags.sql")
    @Sql("/scripts/insert_kanbans.sql")
    @Sql("/scripts/insert_cards.sql")
    void updateNullName() throws Exception {
        long kanbanId = 300;
        Card card = new Card(kanbanId,
                null,
                "newD",
                new Status(100L, "status100"),
                new User(200L, null, null, null),
                List.of(new Tag("tag2"))
        );
        long id = 400L;
        card.setId(id);

        adminRequest(performJson(put("/card"), card)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_statuses.sql")
    @Sql("/scripts/insert_tags.sql")
    @Sql("/scripts/insert_kanbans.sql")
    @Sql("/scripts/insert_cards.sql")
    void updateEmptyDescription() throws Exception {
        long kanbanId = 300;
        Card card = new Card(kanbanId,
                "newCard",
                "",
                new Status(100L, "status100"),
                new User(200L, null, null, null),
                List.of(new Tag("tag2"))
        );
        long id = 400L;
        card.setId(id);

        adminRequest(performJson(put("/card"), card)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_statuses.sql")
    @Sql("/scripts/insert_tags.sql")
    @Sql("/scripts/insert_kanbans.sql")
    @Sql("/scripts/insert_cards.sql")
    void updateNullDescription() throws Exception {
        long kanbanId = 300;
        Card card = new Card(kanbanId,
                "newCard",
                null,
                new Status(100L, "status100"),
                new User(200L, null, null, null),
                List.of(new Tag("tag2"))
        );
        long id = 400L;
        card.setId(id);

        adminRequest(performJson(put("/card"), card)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_statuses.sql")
    @Sql("/scripts/insert_tags.sql")
    @Sql("/scripts/insert_kanbans.sql")
    @Sql("/scripts/insert_cards.sql")
    void updateNullAssignee() throws Exception {
        long kanbanId = 300;
        Card card = new Card(kanbanId,
                "newCard",
                "newD",
                new Status(100L, "status100"),
                null,
                List.of(new Tag("tag2"))
        );
        long id = 400L;
        card.setId(id);

        adminRequest(performJson(put("/card"), card)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_statuses.sql")
    @Sql("/scripts/insert_tags.sql")
    @Sql("/scripts/insert_kanbans.sql")
    @Sql("/scripts/insert_cards.sql")
    void updateNotExistentAssignee() throws Exception {
        long kanbanId = 300;
        Card card = new Card(kanbanId,
                "newCard",
                "newD",
                new Status(100L, "status100"),
                new User(300L, null, null, null),
                List.of(new Tag("tag2"))
        );
        long id = 400L;
        card.setId(id);

        adminRequest(performJson(put("/card"), card)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_statuses.sql")
    @Sql("/scripts/insert_tags.sql")
    @Sql("/scripts/insert_kanbans.sql")
    @Sql("/scripts/insert_cards.sql")
    void updateNullTagList() throws Exception {
        long kanbanId = 300;
        Card card = new Card(kanbanId,
                "newCard",
                "newD",
                new Status(100L, "status100"),
                new User(200L, null, null, null),
                null
        );
        long id = 400L;
        card.setId(id);

        adminRequest(performJson(put("/card"), card)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_statuses.sql")
    @Sql("/scripts/insert_tags.sql")
    @Sql("/scripts/insert_kanbans.sql")
    @Sql("/scripts/insert_cards.sql")
    void updateNullTagName() throws Exception {
        long kanbanId = 300;
        Card card = new Card(kanbanId,
                "newCard",
                "newD",
                new Status(100L, "status100"),
                new User(200L, null, null, null),
                List.of(new Tag(null))
        );
        long id = 400L;
        card.setId(id);

        adminRequest(performJson(put("/card"), card)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_statuses.sql")
    @Sql("/scripts/insert_tags.sql")
    @Sql("/scripts/insert_kanbans.sql")
    @Sql("/scripts/insert_cards.sql")
    void updateEmptyTagName() throws Exception {
        long kanbanId = 300;
        Card card = new Card(kanbanId,
                "newCard",
                "newD",
                new Status(100L, "status100"),
                new User(200L, null, null, null),
                List.of(new Tag(""))
        );
        long id = 400L;
        card.setId(id);

        adminRequest(performJson(put("/card"), card)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_statuses.sql")
    @Sql("/scripts/insert_tags.sql")
    @Sql("/scripts/insert_kanbans.sql")
    @Sql("/scripts/insert_cards.sql")
    void deleteById() throws Exception {
        adminRequest(delete("/card/400")).andExpect(status().isOk());

        assertNotFound(400);
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_statuses.sql")
    @Sql("/scripts/insert_tags.sql")
    @Sql("/scripts/insert_kanbans.sql")
    @Sql("/scripts/insert_cards.sql")
    void deleteByIdNotFound() throws Exception {
        adminRequest(delete("/card/405")).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_statuses.sql")
    @Sql("/scripts/insert_tags.sql")
    @Sql("/scripts/insert_kanbans.sql")
    @Sql("/scripts/insert_cards.sql")
    void deleteByIdTwice() throws Exception {
        adminRequest(delete("/card/400")).andExpect(status().isOk());

        assertNotFound(400);

        adminRequest(delete("/card/400")).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_statuses.sql")
    @Sql("/scripts/insert_tags.sql")
    @Sql("/scripts/insert_kanbans.sql")
    @Sql("/scripts/insert_cards.sql")
    void getById() throws Exception {
        long kanbanId = 300;
        Card card = new Card(kanbanId,
                "card400",
                "d400",
                new Status(100L, "status100"),
                new User(100L, null, null, null),
                List.of(new Tag("tag100"))
        );
        long id = 400L;
        card.setId(id);

        expectCard(adminRequest(get(String.format("/card/%d", id))), card);
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_statuses.sql")
    @Sql("/scripts/insert_tags.sql")
    @Sql("/scripts/insert_kanbans.sql")
    @Sql("/scripts/insert_cards.sql")
    void getByIdNotFound() throws Exception {
        adminRequest(get("/card/410")).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_statuses.sql")
    @Sql("/scripts/insert_tags.sql")
    @Sql("/scripts/insert_kanbans.sql")
    @Sql("/scripts/insert_cards.sql")
    void getByTag() throws Exception {
        long kanbanId = 300;
        Card card400 = new Card(kanbanId,
                "card400",
                "d400",
                new Status(100L, "status100"),
                new User(100L, null, null, null),
                List.of(new Tag(100L, "tag100"))
        );
        card400.setId(400L);

        String content = adminRequest(get("/card/tag/100")).andReturn().getResponse().getContentAsString();
        List<Card> actualCards = objectMapper.readValue(content, new TypeReference<>() {
        });
        actualCards.forEach(card -> {
            card.setCreatedBy(null);
            card.setLastModifiedBy(null);
            card.setAssignee(new User(card.getAssignee().getId(), null, null, null));
        });

        Assertions.assertEquals(List.of(card400), actualCards);
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_statuses.sql")
    @Sql("/scripts/insert_tags.sql")
    @Sql("/scripts/insert_kanbans.sql")
    @Sql("/scripts/insert_cards.sql")
    void getEmptyListByTag() throws Exception {
        adminRequest(get("/card/tag/102"))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(Collections.emptyList())));
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_statuses.sql")
    @Sql("/scripts/insert_tags.sql")
    @Sql("/scripts/insert_kanbans.sql")
    @Sql("/scripts/insert_cards.sql")
    void getByBadTag() throws Exception {
        adminRequest(get("/card/tag/abc")).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_statuses.sql")
    @Sql("/scripts/insert_tags.sql")
    @Sql("/scripts/insert_kanbans.sql")
    @Sql("/scripts/insert_cards.sql")
    void updateStatus() throws Exception {
        long kanbanId = 300;
        Card card = new Card(kanbanId,
                "card400",
                "d400",
                new Status(101L, "status101"),
                new User(100L, null, null, null),
                List.of(new Tag("tag100"))
        );
        long id = 400L;
        card.setId(id);

        adminRequest(performJson(put("/card"), card)).andExpect(status().isOk());

        expectCard(adminRequest(get(String.format("/card/%d", id))), card);
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_statuses.sql")
    @Sql("/scripts/insert_tags.sql")
    @Sql("/scripts/insert_kanbans.sql")
    @Sql("/scripts/insert_cards.sql")
    void updateStatusAndKanban() throws Exception {
        long kanbanId = 301;
        Card card = new Card(kanbanId,
                "card400",
                "d400",
                new Status(101L, "status101"),
                new User(100L, null, null, null),
                List.of(new Tag("tag100"))
        );
        long id = 400L;
        card.setId(id);

        adminRequest(performJson(put("/card"), card)).andExpect(status().isOk());

        expectCard(adminRequest(get(String.format("/card/%d", id))), card);
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_statuses.sql")
    @Sql("/scripts/insert_tags.sql")
    @Sql("/scripts/insert_kanbans.sql")
    @Sql("/scripts/insert_cards.sql")
    void updateNotAvailableStatus() throws Exception {
        long kanbanId = 300;
        Card card = new Card(kanbanId,
                "card400",
                "d400",
                new Status(103L, "status101"),
                new User(100L, null, null, null),
                List.of(new Tag("tag100"))
        );
        long id = 400L;
        card.setId(id);

        adminRequest(performJson(put("/card"), card)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_statuses.sql")
    @Sql("/scripts/insert_tags.sql")
    @Sql("/scripts/insert_kanbans.sql")
    @Sql("/scripts/insert_cards.sql")
    void updateNotAvailableKanban() throws Exception {
        long kanbanId = 302;
        Card card = new Card(kanbanId,
                "card400",
                "d400",
                new Status(101L, "status101"),
                new User(100L, null, null, null),
                List.of(new Tag("tag100"))
        );
        long id = 400L;
        card.setId(id);

        adminRequest(performJson(put("/card"), card)).andExpect(status().is4xxClientError());
    }

    protected void expectCard(ResultActions resultActions, Card expectedCard) throws Exception {
        resultActions.andExpect(status().isOk());

        String content = resultActions.andReturn().getResponse().getContentAsString();
        Card actualCard = objectMapper.readValue(content, Card.class);

        assertEquals(expectedCard, actualCard);
    }

    private void assertEquals(Card expectedCard, Card actualCard) {
        Assertions.assertEquals(expectedCard.getName(), actualCard.getName());
        Assertions.assertEquals(expectedCard.getDescription(), actualCard.getDescription());
        Assertions.assertEquals(expectedCard.getKanbanId(), actualCard.getKanbanId());
        Assertions.assertEquals(getTagNames(expectedCard), getTagNames(actualCard));
        Assertions.assertEquals(expectedCard.getStatus(), actualCard.getStatus());
        Assertions.assertEquals(expectedCard.getAssignee().getId(), actualCard.getAssignee().getId());
    }

    private List<String> getTagNames(Card card) {
        return card.getTagList()
                .stream()
                .map(Tag::getName)
                .toList();
    }

}
