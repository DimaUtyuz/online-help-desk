package ru.tinkoff.fintech.note;

import com.fasterxml.jackson.core.type.TypeReference;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.ResultActions;
import ru.tinkoff.fintech.ControllerTest;
import ru.tinkoff.fintech.note.relation.Relation;
import ru.tinkoff.fintech.tag.Tag;

import java.util.Collections;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class NoteControllerTest extends ControllerTest<Note> {

    static {
        tableName = "notes";
        entityClass = Note.class;
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    void saveWithoutTags() throws Exception {
        long boardId = 100;
        Note note = new Note(boardId, "note1", "d1", Collections.emptyList());
        note.setNextNotes(Collections.emptyList());

        adminRequest(performJson(post("/note"), note)).andExpect(status().isOk());

        long id = getIdByName(note.getName());

        expectNote(adminRequest(get(String.format("/note/%d", id))), note);
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    void saveWithTags() throws Exception {
        long boardId = 100;
        Note note = new Note(boardId, "note1", "d1", List.of(new Tag("tag1"), new Tag("tag2")));
        note.setNextNotes(Collections.emptyList());

        adminRequest(performJson(post("/note"), note)).andExpect(status().isOk());

        long id = getIdByName(note.getName());

        expectNote(adminRequest(get(String.format("/note/%d", id))), note);
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    void saveNonExistentBoard() throws Exception {
        long boardId = 105;
        Note note = new Note(boardId, "note1", "d1", List.of(new Tag("tag1"), new Tag("tag2")));
        note.setNextNotes(Collections.emptyList());

        adminRequest(performJson(post("/note"), note)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    void saveEmptyName() throws Exception {
        long boardId = 100;
        Note note = new Note(boardId, "", "d1", Collections.emptyList());

        adminRequest(performJson(post("/note"), note)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    void saveNullName() throws Exception {
        long boardId = 100;
        Note note = new Note(boardId, null, "d1", Collections.emptyList());

        adminRequest(performJson(post("/note"), note)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    void saveEmptyDescription() throws Exception {
        long boardId = 100;
        Note note = new Note(boardId, "note1", "", Collections.emptyList());

        adminRequest(performJson(post("/note"), note)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    void saveNullDescription() throws Exception {
        long boardId = 100;
        Note note = new Note(boardId, "note1", null, Collections.emptyList());

        adminRequest(performJson(post("/note"), note)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    void saveNullTagList() throws Exception {
        long boardId = 100;
        Note note = new Note(boardId, "note1", "d1", null);

        adminRequest(performJson(post("/note"), note)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    void saveEmptyTagName() throws Exception {
        long boardId = 100;
        Note note = new Note(boardId, "note1", "d1", List.of(new Tag("")));

        adminRequest(performJson(post("/note"), note)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    void saveNullTagName() throws Exception {
        long boardId = 100;
        Note note = new Note(boardId, "note1", "d1", List.of(new Tag(null)));

        adminRequest(performJson(post("/note"), note)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_tags.sql")
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_notes.sql")
    void update() throws Exception {
        long id = 200L;
        long boardId = 100;
        Note note = new Note(boardId, "newNote", "newD", List.of(new Tag("newTag")));

        note.setId(id);

        adminRequest(performJson(put("/note"), note)).andExpect(status().isOk());

        expectNote(adminRequest(get(String.format("/note/%d", id))), note);
    }

    @Test
    @Sql("/scripts/insert_tags.sql")
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_notes.sql")
    void updateNotExistentNote() throws Exception {
        long id = 204L;
        long boardId = 100;
        Note note = new Note(boardId, "newNote", "newD", List.of(new Tag("newTag")));

        note.setId(id);

        adminRequest(performJson(put("/note"), note)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_tags.sql")
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_notes.sql")
    void updateEmptyName() throws Exception {
        long id = 200L;
        long boardId = 100;
        Note note = new Note(boardId, "", "newD", List.of(new Tag("newTag")));

        note.setId(id);

        adminRequest(performJson(put("/note"), note)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_tags.sql")
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_notes.sql")
    void updateNullName() throws Exception {
        long id = 200L;
        long boardId = 100;
        Note note = new Note(boardId, null, "newD", List.of(new Tag("newTag")));

        note.setId(id);

        adminRequest(performJson(put("/note"), note)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_tags.sql")
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_notes.sql")
    void updateEmptyDescription() throws Exception {
        long id = 200L;
        long boardId = 100;
        Note note = new Note(boardId, "newNote", "", List.of(new Tag("newTag")));

        note.setId(id);

        adminRequest(performJson(put("/note"), note)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_tags.sql")
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_notes.sql")
    void updateNullDescription() throws Exception {
        long id = 200L;
        long boardId = 100;
        Note note = new Note(boardId, "newNote", null, List.of(new Tag("newTag")));

        note.setId(id);

        adminRequest(performJson(put("/note"), note)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_tags.sql")
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_notes.sql")
    void updateNullTagList() throws Exception {
        long id = 200L;
        long boardId = 100;
        Note note = new Note(boardId, "newNote", "newD", null);

        note.setId(id);

        adminRequest(performJson(put("/note"), note)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_tags.sql")
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_notes.sql")
    void updateNullTagName() throws Exception {
        long id = 200L;
        long boardId = 100;
        Note note = new Note(boardId, "newNote", "newD", List.of(new Tag(null)));

        note.setId(id);

        adminRequest(performJson(put("/note"), note)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_tags.sql")
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_notes.sql")
    void updateEmptyTagName() throws Exception {
        long id = 200L;
        long boardId = 100;
        Note note = new Note(boardId, "newNote", "newD", List.of(new Tag("")));

        note.setId(id);

        adminRequest(performJson(put("/note"), note)).andExpect(status().is4xxClientError());
    }


    @Test
    @Sql("/scripts/insert_tags.sql")
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_notes.sql")
    void deleteById() throws Exception {
        adminRequest(delete("/note/200")).andExpect(status().isOk());

        assertNotFound(200L);
    }

    @Test
    @Sql("/scripts/insert_tags.sql")
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_notes.sql")
    void deleteByIdNotFound() throws Exception {
        adminRequest(delete("/note/300")).andExpect(status().is4xxClientError());

    }

    @Test
    @Sql("/scripts/insert_tags.sql")
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_notes.sql")
    void deleteByIdTwice() throws Exception {
        adminRequest(delete("/note/200")).andExpect(status().isOk());

        assertNotFound(200L);

        adminRequest(delete("/note/300")).andExpect(status().is4xxClientError());

    }

    @Test
    @Sql("/scripts/insert_tags.sql")
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_notes.sql")
    void getById() throws Exception {
        long id = 200L;
        long boardId = 100;
        Note note = new Note(boardId, "note200", "d200", List.of(new Tag("tag100"), new Tag("tag101")));

        expectNote(adminRequest(get(String.format("/note/%d", id))), note);
    }

    @Test
    @Sql("/scripts/insert_tags.sql")
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_notes.sql")
    void getByIdNotFound() throws Exception {
        long id = 300L;
        adminRequest(get(String.format("/note/%d", id))).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_tags.sql")
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_notes.sql")
    void getAll() throws Exception {
        long boardId = 100;
        Note note200 = new Note(boardId, "note200", "d200", List.of(new Tag(100L, "tag100"), new Tag(101L,"tag101")));
        Note note201 = new Note(boardId, "note201", "d201", List.of(new Tag(100L, "tag100")));
        note200.setId(200L);
        note201.setId(201L);

        String content = adminRequest(get("/note")).andReturn().getResponse().getContentAsString();
        List<Note> actualNotes = objectMapper.readValue(content, new TypeReference<>() {});
        actualNotes.forEach(note -> {note.setCreatedBy(null); note.setLastModifiedBy(null);});

        Assertions.assertEquals(List.of(note200, note201), actualNotes);
    }

    @Test
    void getAllEmpty() throws Exception {
        adminRequest(get("/note"))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(Collections.emptyList())));
    }

    @Test
    @Sql("/scripts/insert_tags.sql")
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_notes.sql")
    void getByTag() throws Exception {
        long boardId = 100;
        Note note200 = new Note(boardId, "note200", "d200", List.of(new Tag(100L, "tag100"), new Tag(101L,"tag101")));
        Note note201 = new Note(boardId, "note201", "d201", List.of(new Tag(100L, "tag100")));
        note200.setId(200L);
        note201.setId(201L);

        String content = adminRequest(get("/note/tag/100")).andReturn().getResponse().getContentAsString();
        List<Note> actualNotes = objectMapper.readValue(content, new TypeReference<>() {});
        actualNotes.forEach(note -> {note.setCreatedBy(null); note.setLastModifiedBy(null);});

        Assertions.assertEquals(List.of(note200, note201), actualNotes);
    }

    @Test
    @Sql("/scripts/insert_tags.sql")
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_notes.sql")
    void getEmptyListByTag() throws Exception {
        adminRequest(get("/note/tag/102"))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(Collections.emptyList())));
    }

    @Test
    @Sql("/scripts/insert_tags.sql")
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_notes.sql")
    void getByBadTag() throws Exception {
        adminRequest(get("/note/tag/abc")).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_tags.sql")
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_notes.sql")
    void addRelation() throws Exception {
        long boardId = 100;
        Note note200 = new Note(boardId, "note200", "d200", List.of(new Tag(100L, "tag100"), new Tag(101L,"tag101")));
        Note note201 = new Note(boardId, "note201", "d201", List.of(new Tag(100L, "tag100")));
        note200.setId(200L);
        note201.setId(201L);

        Relation relation = new Relation(note200.getId(), note201.getId());

        adminRequest(performJson(put("/note/relation/add"), relation)).andExpect(status().isOk());

        note200.getNextNotes().add(note201);

        expectNote(adminRequest(get("/note/200")), note200);
    }

    @Test
    @Sql("/scripts/insert_tags.sql")
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_notes.sql")
    void addRelationWithNotExistentNote() throws Exception {
        Relation relation = new Relation(200L, 300L);

        adminRequest(performJson(put("/note/relation/add"), relation)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_tags.sql")
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_notes.sql")
    void addRelationWithNull() throws Exception {
        Relation relation = new Relation(200L, null);

        adminRequest(performJson(put("/note/relation/add"), relation)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_tags.sql")
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_notes.sql")
    void addNullRelation() throws Exception {

        adminRequest(performJson(put("/note/relation/add"), null)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_tags.sql")
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_notes.sql")
    void deleteRelation() throws Exception {
        long boardId = 100;
        Note note200 = new Note(boardId, "note200", "d200", List.of(new Tag(100L, "tag100"), new Tag(101L,"tag101")));
        Relation relation = new Relation(200L, 201L);

        adminRequest(performJson(put("/note/relation/add"), relation)).andExpect(status().isOk());
        adminRequest(performJson(put("/note/relation/delete"), relation)).andExpect(status().isOk());

        expectNote(adminRequest(get("/note/200")), note200);
    }

    @Test
    @Sql("/scripts/insert_tags.sql")
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_notes.sql")
    void deleteNotExistentRelation() throws Exception {
        Relation relation = new Relation(200L, 300L);

        adminRequest(performJson(put("/note/relation/delete"), relation)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_tags.sql")
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_notes.sql")
    void deleteRelationWithNullNote() throws Exception {
        Relation relation = new Relation(200L, null);

        adminRequest(performJson(put("/note/relation/delete"), relation)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_tags.sql")
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_notes.sql")
    void deleteNullRelation() throws Exception {
        adminRequest(performJson(put("/note/relation/delete"), null)).andExpect(status().is4xxClientError());
    }

    protected void expectNote(ResultActions resultActions, Note expectedNote) throws Exception {
        resultActions.andExpect(status().isOk());

        String content = resultActions.andReturn().getResponse().getContentAsString();
        Note actualNote = objectMapper.readValue(content, Note.class);

        assertEquals(expectedNote, actualNote);
    }

    private void assertEquals(Note expectedNote, Note actualNote) {
        Assertions.assertEquals(expectedNote.getName(), actualNote.getName());
        Assertions.assertEquals(expectedNote.getDescription(), actualNote.getDescription());
        Assertions.assertEquals(expectedNote.getBoardId(), actualNote.getBoardId());
        Assertions.assertEquals(getTagNames(expectedNote), getTagNames(actualNote));
        Assertions.assertEquals(getNoteIds(expectedNote), getNoteIds(actualNote));
    }

    private List<String> getTagNames(Note note) {
        return note.getTagList()
                .stream()
                .map(Tag::getName)
                .toList();
    }

    private List<Long> getNoteIds(Note note) {
        return note.getNextNotes()
                .stream()
                .map(Note::getId)
                .toList();
    }
}
