insert into actions (id, user_id, creation_time)
    values (500, 100, now());

insert into comments (id, name, description, created_id, modified_id, board_id, resolved)
    values (500, 'comment500', 'd500', 500, 500, 100, false);

insert into actions (id, user_id, creation_time)
    values (501, 100, now());

insert into comments (id, name, description, created_id, modified_id, board_id, resolved)
    values (501, 'comment501', 'd501', 501, 501, 100, false);

insert into actions (id, user_id, creation_time)
    values (502, 100, now());

insert into comments (id, name, description, created_id, modified_id, board_id, resolved)
    values (502, 'comment502', 'd502', 502, 502, 100, true);
