package ru.tinkoff.fintech.note.relation;

import org.apache.ibatis.annotations.*;
import ru.tinkoff.fintech.note.Note;

import java.util.List;

@Mapper
public interface RelationRepository {

    @Insert("""
            insert into note_to_note (from_note_id, to_note_id)
                values (#{fromNoteId}, #{toNoteId})
            on conflict do nothing
            """)
    void save(Relation relation);

    @Delete("""
            delete
                from note_to_note
            where from_note_id = #{fromNoteId} AND to_note_id = #{toNoteId}
            """)
    Long delete(Relation relation);

    @Delete("""
            select n.id, n.name, n.description, n.created_id, n.modified_id
                from note_to_note as ntn
            join notes as n on ntn.to_note_id = n.id
            where from_note_id = #{id}
            """)
    @Results(id = "note", value = {
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "createdBy", column = "created_id",
                    one = @One(select = "ru.tinkoff.fintech.action.ActionRepository.findById")),
            @Result(property = "lastModifiedBy", column = "modified_id",
                    one = @One(select = "ru.tinkoff.fintech.action.ActionRepository.findById")),
            @Result(property = "tagList", column = "id",
                    many = @Many(select = "ru.tinkoff.fintech.tag.list.BlockTagsRepository.findTagsByBlockId"))
    })
    List<Note> findNextNotesById(Long id);

}
