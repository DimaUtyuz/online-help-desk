package ru.tinkoff.fintech.kanban.status;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class StatusService {

    private final StatusRepository statusRepository;

    public void add(Status status) {
        statusRepository.save(status);
    }

    public Status getById(Long id) {
        return statusRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Status with id: " + id + " not found"));
    }

    public List<Status> getAll() {
        return statusRepository.findAll();
    }

    @Transactional
    public Status getOrAdd(String name) {
        Optional<Status> statusOptional = statusRepository.findByName(name);
        if (statusOptional.isPresent()) {
            return statusOptional.get();
        } else {
            Status status = new Status(name);
            add(status);
            return status;
        }
    }

    public Status getByName(String name) {
        return statusRepository.findByName(name)
                .orElseThrow(() -> new EntityNotFoundException("Status with name: " + name + " not found"));
    }

}
