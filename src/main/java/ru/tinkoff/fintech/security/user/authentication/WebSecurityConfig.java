package ru.tinkoff.fintech.security.user.authentication;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import ru.tinkoff.fintech.security.role.RoleProperties;
import ru.tinkoff.fintech.security.user.UserService;

import static org.springframework.http.HttpMethod.DELETE;
import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpMethod.POST;
import static org.springframework.http.HttpMethod.PUT;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserService userService;

    @Autowired
    private RoleProperties roleProperties;

    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userService);
    }

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        String adminName = roleProperties.getAdmin().getName();
        String userName = roleProperties.getUser().getName();

        httpSecurity.csrf().disable()
                .authorizeRequests()
                .antMatchers(GET).hasAnyAuthority(userName, adminName)
                .antMatchers(POST).hasAuthority(adminName)
                .antMatchers(PUT).hasAuthority(adminName)
                .antMatchers(DELETE).hasAuthority(adminName)
                .and().httpBasic();
    }

}
