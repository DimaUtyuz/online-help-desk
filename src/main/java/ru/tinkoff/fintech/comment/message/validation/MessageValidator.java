package ru.tinkoff.fintech.comment.message.validation;

import org.springframework.beans.factory.annotation.Autowired;
import ru.tinkoff.fintech.comment.Comment;
import ru.tinkoff.fintech.comment.CommentService;
import ru.tinkoff.fintech.comment.message.Message;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class MessageValidator implements ConstraintValidator<MessageValid, Message> {

    @Autowired
    private CommentService commentService;

    @Override
    public boolean isValid(Message message, ConstraintValidatorContext constraintValidatorContext) {
        Comment comment = commentService.getById(message.getCommentId());

        return !comment.isResolved();
    }

}
