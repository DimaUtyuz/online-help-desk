package ru.tinkoff.fintech.kanban.status.list;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface KanbanStatusesRepository {

    @Insert("""
            insert into status_kanban (status_id, kanban_id)
                values (#{statusId}, #{kanbanId})
                on conflict do nothing
            """)
    void save(Long statusId, Long kanbanId);

    @Select("""
            select exists
                (select 1
                    from status_kanban
                where status_id = #{statusId} AND kanban_id = #{kanbanId}
                )
            """)
    boolean checkStatusExists(Long statusId, Long kanbanId);

    @Delete("""
            delete
                from status_kanban
            where status_id = #{statusId}
                and kanban_id = #{kanbanId}
            """)
    void delete(Long statusId, Long kanbanId);

    @Delete("""
            delete
                from status_kanban
            where kanban_id = #{id}
            """)
    void deleteKanban(Long id);

}
