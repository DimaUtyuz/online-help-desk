package ru.tinkoff.fintech.table;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/table")
@AllArgsConstructor
public class TableController {

    private final TableService tableService;

    @PostMapping
    public void save(@RequestBody @Valid Table table) {
        tableService.add(table);
    }

    @GetMapping("/{id}")
    public Table getById(@PathVariable("id") Long id) {
        return tableService.getById(id);
    }

    @GetMapping
    public List<Table> getAll() {
        return tableService.getAll();
    }

    @PutMapping
    public void update(@RequestBody @Valid Table table) {
        tableService.update(table);
    }

    @PutMapping("/{id}/column/add")
    public void addColumn(@PathVariable("id") Long id) {
        tableService.addColumn(id);
    }

    @PutMapping("/{id}/row/add")
    public void addRow(@PathVariable("id") Long id) {
        tableService.addRow(id);
    }

    @PutMapping("/{id}/column/delete")
    public void deleteColumn(@PathVariable("id") Long id) {
        tableService.deleteColumn(id);
    }

    @PutMapping("/{id}/row/delete")
    public void deleteRow(@PathVariable("id") Long id) {
        tableService.deleteRow(id);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") Long id) {
        tableService.delete(id);
    }

}
