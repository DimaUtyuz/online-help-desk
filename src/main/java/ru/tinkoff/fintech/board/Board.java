package ru.tinkoff.fintech.board;

import lombok.*;
import ru.tinkoff.fintech.block.Block;
import ru.tinkoff.fintech.comment.Comment;
import ru.tinkoff.fintech.form.Form;
import ru.tinkoff.fintech.kanban.Kanban;
import ru.tinkoff.fintech.note.Note;
import ru.tinkoff.fintech.table.Table;

import java.util.Collections;
import java.util.List;

/**
 * Space where {@link Form} can be placed
 */
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Board extends Block {

    private List<Table> tables;
    private List<Kanban> kanbans;
    private List<Note> notes;
    private List<Comment> comments;

    public Board(String name, String description) {
        this.name = name;
        this.description = description;
        this.tables = Collections.emptyList();
        this.kanbans = Collections.emptyList();
        this.notes = Collections.emptyList();
        this.comments = Collections.emptyList();
    }

}
