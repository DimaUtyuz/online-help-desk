package ru.tinkoff.fintech.board;

import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Optional;

@Mapper
public interface BoardRepository {

    @Insert("""
            insert into boards (name, description, created_id, modified_id)
                values (#{name}, #{description}, #{createdBy.id}, #{lastModifiedBy.id})
            """)
    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
    void save(Board board);

    @Select("""
            select b.id, b.name, b.description, b.created_id, b.modified_id
                from boards as b
            where b.id = #{id}
            """)
    @Results(id = "board", value = {
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "createdBy", column = "created_id",
                    one = @One(select = "ru.tinkoff.fintech.action.ActionRepository.findById")),
            @Result(property = "lastModifiedBy", column = "modified_id",
                    one = @One(select = "ru.tinkoff.fintech.action.ActionRepository.findById")),
            @Result(property = "kanbans", column = "id",
                    one = @One(select = "ru.tinkoff.fintech.kanban.KanbanRepository.findByBoardId")),
            @Result(property = "comments", column = "id",
                    one = @One(select = "ru.tinkoff.fintech.comment.CommentRepository.findByBoardId")),
            @Result(property = "tables", column = "id",
                    one = @One(select = "ru.tinkoff.fintech.table.TableRepository.findByBoardId")),
            @Result(property = "notes", column = "id",
                    one = @One(select = "ru.tinkoff.fintech.note.NoteRepository.findByBoardId"))
    })
    Optional<Board> findById(Long id);

    @Select("""
            select exists
                (select 1
                    from boards
                where id = #{id}
                )
            """)
    boolean isExistById(Long id);

    @Select("""
            select b.id, b.name, b.description, b.created_id, b.modified_id
                from boards as b
            """)
    @ResultMap("board")
    List<Board> findAll();

    @Update("""
            update boards
                set name = #{name}, description = #{description}, modified_id = #{lastModifiedBy.id}
            where id = #{id}
            """)
    Long update(Board board);

    @Delete("""
            delete
                from boards
            where id = #{id}
            """)
    Long delete(Long id);

}
