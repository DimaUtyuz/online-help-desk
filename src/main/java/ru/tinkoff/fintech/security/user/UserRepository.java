package ru.tinkoff.fintech.security.user;

import org.apache.ibatis.annotations.*;

import java.util.Optional;

@Mapper
public interface UserRepository {

    @Insert("""
            insert into users (login, password, role_id)
                values (#{login}, #{password}, #{role.id})
            """)
    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
    void save(User user);

    @Select("""
            select u.id, u.login, u.password, u.role_id
                from users as u
            where u.login = #{login}
            """)
    @Results(id = "user", value = {
            @Result(property = "id", column = "id"),
            @Result(property = "login", column = "login"),
            @Result(property = "password", column = "password"),
            @Result(property = "role", column = "role_id",
                    one = @One(select = "ru.tinkoff.fintech.security.role.RoleRepository.findById"))
    })
    Optional<User> findByLogin(String login);

    @Select("""
            select u.id, u.login, u.role_id
                from users as u
            where u.id = #{id}
            """)
    @Results(id = "userDto", value = {
            @Result(property = "id", column = "id"),
            @Result(property = "login", column = "login"),
            @Result(property = "role", column = "role_id",
                    one = @One(select = "ru.tinkoff.fintech.security.role.RoleRepository.findById"))
    })
    Optional<User> findByIdWithoutPassword(Long id);

    @Select("""
            select u.id, u.login, u.password, u.role_id
                from users as u
            where u.id = #{id}
            """)
    @ResultMap("user")
    Optional<User> findById(Long id);

    @Select("""
            select exists
                (select 1
                    from users
                where id = #{id}
                )
            """)
    boolean isExistById(Long id);

}
