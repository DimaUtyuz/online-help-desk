insert into actions (id, user_id, creation_time)
    values (800, 100, now());

insert into cells (id, table_id, "column", "row", "text")
    values (800, 700, 1, 1, 'text11');

insert into actions (id, user_id, creation_time)
values (801, 100, now());

insert into cells (id, table_id, "column", "row", "text")
    values (801, 700, 1, 2, 'text12');

insert into actions (id, user_id, creation_time)
    values (802, 100, now());

insert into cells (id, table_id, "column", "row", "text")
    values (802, 700, 2, 1, 'text21');

insert into actions (id, user_id, creation_time)
    values (803, 100, now());

insert into cells (id, table_id, "column", "row", "text")
    values (803, 700, 2, 2, 'text22');

insert into actions (id, user_id, creation_time)
    values (804, 100, now());

insert into cells (id, table_id, "column", "row", "text")
    values (804, 701, 1, 1, 'text11');
