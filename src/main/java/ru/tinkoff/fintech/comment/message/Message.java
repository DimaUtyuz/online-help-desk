package ru.tinkoff.fintech.comment.message;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.tinkoff.fintech.action.Action;
import ru.tinkoff.fintech.comment.Comment;
import ru.tinkoff.fintech.comment.message.validation.MessageValid;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * {@link Comment} element that behaves as a message from chat
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@MessageValid
public class Message {

    private Long id;

    @NotNull(message = "Комментарий не задан")
    private Long commentId;

    @NotBlank(message = "Текст сообщения пуст")
    private String text;

    private Action createdBy;

    public Message(Long commentId, String text) {
        this.commentId = commentId;
        this.text = text;
    }

}
