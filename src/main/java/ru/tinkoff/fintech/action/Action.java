package ru.tinkoff.fintech.action;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.tinkoff.fintech.security.user.User;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Action {

    private Long id;

    @NotNull(message = "Автор не определён.")
    private User author;

    private Date actionTime;

    public Action(User author) {
        this.author = author;
    }
}
