insert into actions (id, user_id, creation_time)
    values (100, 100, now());

insert into boards (id, name, description, created_id, modified_id)
    values (100, 'board100', 'd100', 100, 100);

insert into actions (id, user_id, creation_time)
    values (101, 100, now());

insert into boards (id, name, description, created_id, modified_id)
    values (101, 'board101', 'd101', 101, 101);
