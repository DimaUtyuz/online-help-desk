package ru.tinkoff.fintech.kanban.status;

import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Optional;

@Mapper
public interface StatusRepository {

    @Insert("insert into statuses (name) values (#{name})")
    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
    void save(Status status);

    @Select("""
            select s.id, s.name
                from statuses as s
            where s.id = #{id}
            """)
    @Results(id = "status", value = {
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name")
    })
    Optional<Status> findById(Long id);

    @Select("""
            select s.id, s.name
                from statuses as s
            where s.name = #{name}
            """)
    @ResultMap("status")
    Optional<Status> findByName(String name);

    @Select("""
            select s.id, s.name
                from statuses as s
            """)
    @ResultMap("status")
    List<Status> findAll();

    @Select("""
            select s.id, s.name
                from status_kanban as sk
            join statuses as s on sk.status_id = s.id
            where sk.kanban_id = #{kanbanId}
            """)
    @ResultMap("status")
    List<Status> getByKanbanId(Long kanbanId);

}
