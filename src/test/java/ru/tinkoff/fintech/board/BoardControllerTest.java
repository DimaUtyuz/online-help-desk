package ru.tinkoff.fintech.board;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.ResultActions;
import ru.tinkoff.fintech.ControllerTest;
import ru.tinkoff.fintech.comment.Comment;
import ru.tinkoff.fintech.kanban.Kanban;
import ru.tinkoff.fintech.note.Note;
import ru.tinkoff.fintech.table.Table;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class BoardControllerTest extends ControllerTest<Board> {

    static {
        tableName = "boards";
        entityClass = Board.class;
    }

    @Test
    void save() throws Exception {
        Board board = new Board("board100", "d100");

        adminRequest(performJson(post("/board"), board)).andExpect(status().isOk());

        long id = getIdByName(board.getName());

        expectBoard(adminRequest(get(String.format("/board/%d", id))), board);
    }

    @Test
    void saveEmptyName() throws Exception {
        Board board = new Board("", "d100");

        adminRequest(performJson(post("/board"), board)).andExpect(status().is4xxClientError());
    }
    @Test

    void saveNullName() throws Exception {
        Board board = new Board(null, "d100");

        adminRequest(performJson(post("/board"), board)).andExpect(status().is4xxClientError());
    }

    @Test
    void saveEmptyDescription() throws Exception {
        Board board = new Board("board100", "");

        adminRequest(performJson(post("/board"), board)).andExpect(status().is4xxClientError());
    }

    @Test
    void saveNulLDescription() throws Exception {
        Board board = new Board("board100", null);

        adminRequest(performJson(post("/board"), board)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    void update() throws Exception {
        long id = 100;
        Board board = new Board("newBoard", "newD");
        board.setId(id);

        adminRequest(performJson(put("/board"), board)).andExpect(status().isOk());

        expectBoard(adminRequest(get(String.format("/board/%d", id))), board);
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    void updateEmptyName() throws Exception {
        long id = 100;
        Board board = new Board("", "newD");
        board.setId(id);

        adminRequest(performJson(put("/board"), board)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    void updateNullName() throws Exception {
        long id = 100;
        Board board = new Board(null, "newD");
        board.setId(id);

        adminRequest(performJson(put("/board"), board)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    void updateEmptyDescription() throws Exception {
        long id = 100;
        Board board = new Board("newBoard", "");
        board.setId(id);

        adminRequest(performJson(put("/board"), board)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    void updateNullDescription() throws Exception {
        long id = 100;
        Board board = new Board("newBoard", null);
        board.setId(id);

        adminRequest(performJson(put("/board"), board)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    void updateNonExistentBoard() throws Exception {
        long id = 110;
        Board board = new Board("newBoard", "newD");
        board.setId(id);

        adminRequest(performJson(put("/board"), board)).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_statuses.sql")
    @Sql("/scripts/insert_tags.sql")
    @Sql("/scripts/insert_kanbans.sql")
    @Sql("/scripts/insert_tables.sql")
    @Sql("/scripts/insert_notes.sql")
    @Sql("/scripts/insert_comments.sql")
    void getById() throws Exception {
        long id = 100;
        Board board = new Board("board100", "d100");
        board.setId(id);
        board.setComments(List.of(createCommentById(500), createCommentById(501), createCommentById(502)));
        board.setNotes(List.of(createNoteById(200), createNoteById(201)));
        board.setTables(List.of(createTableById(700), createTableById(701)));
        board.setKanbans(List.of(createKanbanById(300), createKanbanById(301)));

        expectBoard(adminRequest(get(String.format("/board/%d", id))), board);
    }

    @Test
    void getByIdNotFound() throws Exception {
        long id = 110;
        adminRequest(get(String.format("/board/%d", id))).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_statuses.sql")
    @Sql("/scripts/insert_tags.sql")
    @Sql("/scripts/insert_kanbans.sql")
    @Sql("/scripts/insert_cards.sql")
    @Sql("/scripts/insert_tables.sql")
    @Sql("/scripts/insert_cells.sql")
    @Sql("/scripts/insert_notes.sql")
    @Sql("/scripts/insert_comments.sql")
    @Sql("/scripts/insert_messages.sql")
    void deleteById() throws Exception {
        long id = 100;
        adminRequest(delete(String.format("/board/%d", id))).andExpect(status().isOk());

        checkDelete(id);
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_statuses.sql")
    @Sql("/scripts/insert_tags.sql")
    @Sql("/scripts/insert_kanbans.sql")
    @Sql("/scripts/insert_cards.sql")
    @Sql("/scripts/insert_tables.sql")
    @Sql("/scripts/insert_cells.sql")
    @Sql("/scripts/insert_notes.sql")
    @Sql("/scripts/insert_comments.sql")
    @Sql("/scripts/insert_messages.sql")
    void deleteByIdNotFound() throws Exception {
        long id = 110;
        adminRequest(delete(String.format("/board/%d", id))).andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/scripts/insert_boards.sql")
    @Sql("/scripts/insert_statuses.sql")
    @Sql("/scripts/insert_tags.sql")
    @Sql("/scripts/insert_kanbans.sql")
    @Sql("/scripts/insert_cards.sql")
    @Sql("/scripts/insert_tables.sql")
    @Sql("/scripts/insert_cells.sql")
    @Sql("/scripts/insert_notes.sql")
    @Sql("/scripts/insert_comments.sql")
    @Sql("/scripts/insert_messages.sql")
    void deleteByIdTwice() throws Exception {
        long id = 100;
        adminRequest(delete(String.format("/board/%d", id))).andExpect(status().isOk());

        checkDelete(id);

        adminRequest(delete(String.format("/board/%d", id))).andExpect(status().is4xxClientError());
    }

    private void checkDelete(long id) {
        assertNotFound(id);
        assertEmpty("kanbans", "board_id", id);
        assertEmpty("comments", "board_id", id);
        assertEmpty("tables", "board_id", id);
        assertEmpty("notes", "board_id", id);
    }

    private Comment createCommentById(long id) {
        Comment comment = new Comment();
        comment.setId(id);
        return comment;
    }

    private Note createNoteById(long id) {
        Note note = new Note();
        note.setId(id);
        return note;
    }

    private Table createTableById(long id) {
        Table table = new Table();
        table.setId(id);
        return table;
    }

    private Kanban createKanbanById(long id) {
        Kanban kanban = new Kanban();
        kanban.setId(id);
        return kanban;
    }

    protected void expectBoard(ResultActions resultActions, Board expectedBoard) throws Exception {
        resultActions.andExpect(status().isOk());

        String content = resultActions.andReturn().getResponse().getContentAsString();
        Board actualBoard = objectMapper.readValue(content, Board.class);

        assertEquals(expectedBoard, actualBoard);
    }

    private void assertEquals(Board expectedBoard, Board actualBoard) {
        Assertions.assertEquals(expectedBoard.getName(), actualBoard.getName());
        Assertions.assertEquals(expectedBoard.getDescription(), actualBoard.getDescription());
        Assertions.assertEquals(getNoteIds(expectedBoard), getNoteIds(actualBoard));
        Assertions.assertEquals(getCommentIds(expectedBoard), getCommentIds(actualBoard));
        Assertions.assertEquals(getTableIds(expectedBoard), getTableIds(actualBoard));
        Assertions.assertEquals(getKanbanIds(expectedBoard), getKanbanIds(actualBoard));
    }

    private Set<Long> getNoteIds(Board board) {
        return board.getNotes()
                .stream()
                .map(Note::getId)
                .collect(Collectors.toSet());
    }

    private Set<Long> getCommentIds(Board board) {
        return board.getComments()
                .stream()
                .map(Comment::getId)
                .collect(Collectors.toSet());
    }

    private Set<Long> getTableIds(Board board) {
        return board.getTables()
                .stream()
                .map(Table::getId)
                .collect(Collectors.toSet());
    }

    private Set<Long> getKanbanIds(Board board) {
        return board.getKanbans()
                .stream()
                .map(Kanban::getId)
                .collect(Collectors.toSet());
    }

}
