package ru.tinkoff.fintech.form;

import ru.tinkoff.fintech.board.Board;

/**
 * Element that can be placed on the {@link Board}
 */
public interface Form {

    Long getBoardId();

}
