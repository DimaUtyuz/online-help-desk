package ru.tinkoff.fintech.note;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.tinkoff.fintech.note.relation.Relation;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/note")
@AllArgsConstructor
public class NoteController {

    private final NoteService noteService;

    @PostMapping
    public void save(@RequestBody @Valid Note note) {
        noteService.add(note);
    }

    @GetMapping("/{id}")
    public Note getById(@PathVariable("id") Long id) {
        return noteService.getById(id);
    }

    @GetMapping("/tag/{tagId}")
    public List<Note> getByTagId(@PathVariable("tagId") Long tagId) {
        return noteService.getByTagId(tagId);
    }

    @GetMapping
    public List<Note> getAll() {
        return noteService.getAll();
    }

    @PutMapping
    public void update(@RequestBody @Valid Note note) {
        noteService.update(note);
    }

    @PutMapping("/relation/add")
    public void addRelation(@RequestBody @Valid Relation relation) {
        noteService.addRelation(relation);
    }

    @PutMapping("/relation/delete")
    public void deleteRelation(@RequestBody @Valid Relation relation) {
        noteService.deleteRelation(relation);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") Long id) {
        noteService.delete(id);
    }

}
