package ru.tinkoff.fintech.table;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tinkoff.fintech.action.aop.annotation.CreateBlockBy;
import ru.tinkoff.fintech.action.aop.annotation.ModifyBlockBy;
import ru.tinkoff.fintech.table.cell.Cell;
import ru.tinkoff.fintech.table.cell.CellService;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
@AllArgsConstructor
public class TableService {

    private final TableRepository tableRepository;
    private final CellService cellService;

    @Transactional
    @CreateBlockBy
    public void add(Table table) {
        tableRepository.save(table);
        cellService.addCells(table);
    }

    public Table getById(Long id) {
        return tableRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Table with id: " + id + " not found"));
    }

    public List<Table> getAll() {
        return tableRepository.findAll();
    }

    @Transactional
    public void addColumn(Long id) {
        Table table = getById(id);
        table.setWidth(table.getWidth() + 1);
        updateSize(table);

        int column = table.getWidth();
        for (int i = 0; i < table.getHeight(); i++) {
            Cell cell = new Cell(column, i, "");
            cellService.add(cell, table.getId());
        }
    }

    @Transactional
    public void addRow(Long id) {
        Table table = getById(id);
        table.setHeight(table.getHeight() + 1);
        updateSize(table);

        int row = table.getHeight();
        for (int i = 0; i < table.getWidth(); i++) {
            Cell cell = new Cell(i, row, "");
            cellService.add(cell, table.getId());
        }
    }

    @Transactional
    public void deleteColumn(Long id) {
        Table table = getById(id);
        int width = table.getWidth();
        if (width > 1) {
            cellService.deleteColumn(id, width);

            table.setWidth(width - 1);
            updateSize(table);
        } else {
            throw new IllegalArgumentException(String.format("Table with id: %d has only 1 column", id));
        }
    }

    @Transactional
    public void deleteRow(Long id) {
        Table table = getById(id);
        int height = table.getHeight();
        if (height > 1) {
            cellService.deleteRow(id, height);

            table.setHeight(height - 1);
            updateSize(table);
        } else {
            throw new IllegalArgumentException(String.format("Table with id: %d has only 1 row", id));
        }
    }

    @Transactional
    public void delete(Long id) {
        if (tableRepository.delete(id) == 0) {
            throw new EntityNotFoundException("Table with id: " + id + " not found");
        }
    }

    @Transactional
    @ModifyBlockBy
    public void update(Table table) {
        if (tableRepository.update(table) == 0) {
            throw new EntityNotFoundException("Table with id: " + table.getId() + " not found");
        }
    }

    @Transactional
    @ModifyBlockBy
    public void updateSize(Table table) {
        if (tableRepository.updateSize(table) == 0) {
            throw new EntityNotFoundException("Table with id: " + table.getId() + " not found");
        }
    }

}
