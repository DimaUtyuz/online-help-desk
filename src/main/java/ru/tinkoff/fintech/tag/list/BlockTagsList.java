package ru.tinkoff.fintech.tag.list;

import lombok.AllArgsConstructor;
import lombok.Data;
import ru.tinkoff.fintech.tag.Tag;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@AllArgsConstructor
public class BlockTagsList {

    @NotNull(message = "Набор тегов не задан.")
    private List<Tag> tagList;

    @NotNull(message = "Блок не определён.")
    private Long blockId;

}
