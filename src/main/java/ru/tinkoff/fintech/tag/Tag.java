package ru.tinkoff.fintech.tag;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Tag {

    private Long id;

    @NotBlank(message = "Тег пуст.")
    private String name;

    public Tag(String name) {
        this.name = name;
    }
}
