package ru.tinkoff.fintech.kanban;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/kanban")
@AllArgsConstructor
public class KanbanController {

    private final KanbanService kanbanService;

    @PostMapping
    public void save(@RequestBody @Valid Kanban kanban) {
        kanbanService.add(kanban);
    }

    @GetMapping("/{id}")
    public Kanban getById(@PathVariable("id") Long id) {
        return kanbanService.getById(id);
    }

    @GetMapping
    public List<Kanban> getAll() {
        return kanbanService.getAll();
    }

    @PutMapping
    public void update(@RequestBody @Valid Kanban kanban) {
        kanbanService.update(kanban);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") Long id) {
        kanbanService.delete(id);
    }

}
