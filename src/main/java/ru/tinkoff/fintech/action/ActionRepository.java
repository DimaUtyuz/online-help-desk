package ru.tinkoff.fintech.action;

import org.apache.ibatis.annotations.*;

import java.util.Optional;

@Mapper
public interface ActionRepository {

    @Insert("insert into actions (user_id, creation_time) values (#{author.id}, now())")
    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
    void save(Action action);

    @Select("""
            select a.id, a.user_id, a.creation_time
                from actions as a
            where a.id = #{id}
            """)
    @Results(id = "action", value = {
            @Result(property = "id", column = "id"),
            @Result(property = "actionTime", column = "creation_time"),
            @Result(property = "author", column = "user_id",
                    one = @One(select = "ru.tinkoff.fintech.security.user.UserRepository.findByIdWithoutPassword"))
    })
    Optional<Action> findById(Long id);

}
