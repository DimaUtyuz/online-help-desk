package ru.tinkoff.fintech.comment.message;

import lombok.AllArgsConstructor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tinkoff.fintech.action.Action;
import ru.tinkoff.fintech.action.ActionService;
import ru.tinkoff.fintech.action.aop.annotation.CreateMessageBy;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
@AllArgsConstructor
public class MessageService {

    private final MessageRepository messageRepository;

    @Transactional
    @CreateMessageBy
    public void add(Message message) {;
        messageRepository.save(message);
    }

    public Message getById(Long id) {
        return messageRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Message with id: " + id + " not found"));
    }

    public List<Message> getByCommentId(Long commentId) {
        return messageRepository.findMessagesByCommentId(commentId);
    }

    @Transactional
    public void update(Message message) {
        checkAccessToMessage(message.getId());
        if (messageRepository.update(message) == 0) {
            throw new EntityNotFoundException("Message with id: " + message.getId() + " not found");
        }
    }

    @Transactional
    public void delete(Long id) {
        checkAccessToMessage(id);
        if (messageRepository.delete(id) == 0) {
            throw new EntityNotFoundException("Message with id: " + id + " not found");
        }
    }

    private void checkAccessToMessage(Long messageId) {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        Message message = getById(messageId);
        String author = message.getCreatedBy().getAuthor().getLogin();

        if (!author.equals(username)) {
            throw new IllegalArgumentException(
                    String.format("User: %s cannot change message from another user: %s", username, author)
            );
        }
    }

}
