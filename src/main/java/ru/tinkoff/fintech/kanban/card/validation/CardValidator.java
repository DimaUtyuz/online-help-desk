package ru.tinkoff.fintech.kanban.card.validation;

import org.springframework.beans.factory.annotation.Autowired;
import ru.tinkoff.fintech.kanban.card.Card;
import ru.tinkoff.fintech.kanban.status.list.KanbanStatusesService;
import ru.tinkoff.fintech.security.user.User;
import ru.tinkoff.fintech.security.user.UserService;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class CardValidator implements ConstraintValidator<CardValid, Card> {

    @Autowired
    private KanbanStatusesService kanbanStatusesService;

    @Autowired
    private UserService userService;

    @Override
    public boolean isValid(Card card, ConstraintValidatorContext constraintValidatorContext) {
        User assignee = card.getAssignee();
        if (!userService.isExist(assignee)) {
            return false;
        }
        if (card.getStatus() == null) {
            return false;
        }
        Long kanbanId = card.getKanbanId();
        Long statusId = card.getStatus().getId();
        return kanbanStatusesService.isStatusExist(statusId, kanbanId);
    }

}
