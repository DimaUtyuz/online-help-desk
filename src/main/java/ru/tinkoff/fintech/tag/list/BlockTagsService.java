package ru.tinkoff.fintech.tag.list;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tinkoff.fintech.block.Block;
import ru.tinkoff.fintech.tag.Tag;
import ru.tinkoff.fintech.tag.TagService;

import java.util.List;

@Service
@AllArgsConstructor
public class BlockTagsService {

    private final BlockTagsRepository blockTagsRepository;
    private final TagService tagService;

    @Transactional
    public void addBlockTags(BlockTagsList blockTagsList) {
        List<Tag> tagsList = blockTagsList.getTagList();
        tagsList.replaceAll(tag -> tagService.getOrAdd(tag.getName()));

        Long blockId = blockTagsList.getBlockId();

        blockTagsRepository.deleteBlock(blockId);
        for (Tag tag : tagsList) {
            blockTagsRepository.save(tag.getId(), blockId);
        }
    }

    public void deleteBlock(Long blockId) {
        blockTagsRepository.deleteBlock(blockId);
    }

    public void delete(Long tagId, Long blockId) {
        blockTagsRepository.delete(tagId, blockId);
    }

    public List<Tag> getTagsByBlockId(Long id) {
        return blockTagsRepository.findTagsByBlockId(id);
    }

    public List<Long> getBlockIdsByTagId(Long id) {
        return blockTagsRepository.findBlockIdsByTagId(id);
    }

}
