ALTER TABLE comments DROP CONSTRAINT comments_board_id_fkey;
ALTER TABLE comments ADD FOREIGN KEY (board_id) REFERENCES boards(id) ON DELETE CASCADE;

ALTER TABLE messages DROP CONSTRAINT messages_comment_id_fkey;
ALTER TABLE messages ADD FOREIGN KEY (comment_id) REFERENCES comments(id) ON DELETE CASCADE;

ALTER TABLE tables DROP CONSTRAINT tables_board_id_fkey;
ALTER TABLE tables ADD FOREIGN KEY (board_id) REFERENCES boards(id) ON DELETE CASCADE;

ALTER TABLE cells DROP CONSTRAINT cells_table_id_fkey;
ALTER TABLE cells ADD FOREIGN KEY (table_id) REFERENCES tables(id) ON DELETE CASCADE;

ALTER TABLE kanbans DROP CONSTRAINT kanbans_board_id_fkey;
ALTER TABLE kanbans ADD FOREIGN KEY (board_id) REFERENCES boards(id) ON DELETE CASCADE;

ALTER TABLE cards DROP CONSTRAINT cards_kanban_id_fkey;
ALTER TABLE cards ADD FOREIGN KEY (kanban_id) REFERENCES kanbans(id) ON DELETE CASCADE;

ALTER TABLE status_kanban DROP CONSTRAINT status_kanban_kanban_id_fkey;
ALTER TABLE status_kanban ADD FOREIGN KEY (kanban_id) REFERENCES kanbans(id) ON DELETE CASCADE;

ALTER TABLE notes DROP CONSTRAINT notes_board_id_fkey;
ALTER TABLE notes ADD FOREIGN KEY (board_id) REFERENCES boards(id) ON DELETE CASCADE;

ALTER TABLE note_to_note DROP CONSTRAINT note_to_note_from_note_id_fkey;
ALTER TABLE note_to_note ADD FOREIGN KEY (from_note_id) REFERENCES notes(id) ON DELETE CASCADE;

ALTER TABLE note_to_note DROP CONSTRAINT note_to_note_to_note_id_fkey;
ALTER TABLE note_to_note ADD FOREIGN KEY (to_note_id) REFERENCES notes(id) ON DELETE CASCADE;
