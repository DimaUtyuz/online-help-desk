insert into actions (id, user_id, creation_time)
    values (600, 100, now());

insert into messages(id, comment_id, text, created_id)
    values (600, 500, 'message600', 600);

insert into actions (id, user_id, creation_time)
    values (601, 100, now());

insert into messages(id, comment_id, text, created_id)
    values (601, 502, 'message601', 601);

insert into actions (id, user_id, creation_time)
    values (602, 100, now());

insert into messages(id, comment_id, text, created_id)
    values (602, 500, 'message602', 602);
