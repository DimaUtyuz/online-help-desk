package ru.tinkoff.fintech.comment.message.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = MessageValidator.class)
@Target({ElementType.TYPE, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
public @interface MessageValid {

    String message() default "You can not add or update message from resolved comment";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
