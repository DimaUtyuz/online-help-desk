package ru.tinkoff.fintech.note;

import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Optional;

@Mapper
public interface NoteRepository {

    @Insert("""
            insert into notes (name, description, created_id, modified_id, board_id)
                values (#{name}, #{description}, #{createdBy.id}, #{lastModifiedBy.id}, #{boardId})
            """)
    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
    void save(Note note);

    @Select("""
            select n.id, n.name, n.description, n.created_id, n.modified_id, n.board_id
                from notes as n
            where n.id = #{id}
            """)
    @Results(id = "note", value = {
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "boardId", column = "board_id"),
            @Result(property = "createdBy", column = "created_id",
                    one = @One(select = "ru.tinkoff.fintech.action.ActionRepository.findById")),
            @Result(property = "lastModifiedBy", column = "modified_id",
                    one = @One(select = "ru.tinkoff.fintech.action.ActionRepository.findById")),
            @Result(property = "nextNotes", column = "id",
                    many = @Many(select = "ru.tinkoff.fintech.note.relation.RelationRepository.findNextNotesById")),
            @Result(property = "tagList", column = "id",
                    many = @Many(select = "ru.tinkoff.fintech.tag.list.BlockTagsRepository.findTagsByBlockId"))
    })
    Optional<Note> findById(Long id);

    @Select("""
            select n.id, n.name, n.description, n.created_id, n.modified_id, n.board_id
                from notes as n
            """)
    @ResultMap("note")
    List<Note> findAll();

    @Select("""
            select n.id, n.name, n.description, n.created_id, n.modified_id, n.board_id
                from notes as n
            where n.board_id = #{boardId}
            """)
    @ResultMap("note")
    List<Note> findByBoardId(Long boardId);

    @Update("""
            update notes
                set name = #{name}, description = #{description}, modified_id = #{lastModifiedBy.id}
            where id = #{id}
            """)
    Long update(Note note);

    @Delete("""
            delete
                from notes
            where id = #{id}
            """)
    Long delete(Long id);

}
